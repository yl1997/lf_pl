package com.lfot.erp.task;

import com.lfot.erp.task.service.SaleOrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class LfotErpTaskApplicationTests {

	@Autowired
	private SaleOrderService saleOrderTaskService;


	@Test
	void contextLoads() throws Exception {
//		String data = "<![CDATA[<?xml version=\"1.0\" encoding=\"utf-8\"?><BODY><HEADER><TRANSACTION_ID>6365780</TRANSACTION_ID><SUPPLIER_CODE>103438</SUPPLIER_CODE><XSOURCE_NO>PO202210311</XSOURCE_NO><EMPLOYEE_CODE>10129</EMPLOYEE_CODE><DOC_DATE>2022/10/31</DOC_DATE><PURCHASE_DATE>2022/10/31</PURCHASE_DATE><CURRENCY_CODE>CNY</CURRENCY_CODE><SUPPLIER_ADDR_NAME>孝感镇共和村6组</SUPPLIER_ADDR_NAME><SUPPLIER_CONTACT_NAME>龙波</SUPPLIER_CONTACT_NAME><PURCHASE_TYPE>1</PURCHASE_TYPE><REMARK></REMARK><REV_ORG_SOURCE_ID.RTK>PLANT</REV_ORG_SOURCE_ID.RTK><REV_ORG_SOURCE_CODE>980</REV_ORG_SOURCE_CODE><PLANT_ADDRESS>四川峨眉山市车箭路2号</PLANT_ADDRESS><SOURCE_SUPPLIER_CODE>103438</SOURCE_SUPPLIER_CODE><INVOICE_SUPPLIER_CODE>103438</INVOICE_SUPPLIER_CODE><TAX_CODE></TAX_CODE><LINE><UDF023>1-1</UDF023><XSOURCE_SEQ>810258</XSOURCE_SEQ><ITEM_CODE>101002</ITEM_CODE><BUSINESS_QTY>100000</BUSINESS_QTY><BUSINESS_UNIT_CODE>KM</BUSINESS_UNIT_CODE><PRICE_QTY>100000</PRICE_QTY><PRICE_UNIT_CODE>KM</PRICE_UNIT_CODE><PRICE>27.1368</PRICE><PLAN_ARRIVAL_DATE>2022-11-03 09:29:52</PLAN_ARRIVAL_DATE><AMOUNT>2713680</AMOUNT></LINE><LINE><UDF023>1-1</UDF023><XSOURCE_SEQ>810258</XSOURCE_SEQ><ITEM_CODE>101002</ITEM_CODE><BUSINESS_QTY>100000</BUSINESS_QTY><BUSINESS_UNIT_CODE>KM</BUSINESS_UNIT_CODE><PRICE_QTY>100000</PRICE_QTY><PRICE_UNIT_CODE>KM</PRICE_UNIT_CODE><PRICE>27.1368</PRICE><PLAN_ARRIVAL_DATE>2022-11-03 09:29:52</PLAN_ARRIVAL_DATE><AMOUNT>2713680</AMOUNT></LINE></HEADER> <HEADER><TRANSACTION_ID>6365780</TRANSACTION_ID><SUPPLIER_CODE>103438</SUPPLIER_CODE><XSOURCE_NO>PO202210312</XSOURCE_NO><EMPLOYEE_CODE>10129</EMPLOYEE_CODE><DOC_DATE>2022/10/31</DOC_DATE><PURCHASE_DATE>2022/10/31</PURCHASE_DATE><CURRENCY_CODE>CNY</CURRENCY_CODE><SUPPLIER_ADDR_NAME>孝感镇共和村6组</SUPPLIER_ADDR_NAME><SUPPLIER_CONTACT_NAME>龙波</SUPPLIER_CONTACT_NAME><PURCHASE_TYPE>1</PURCHASE_TYPE><REMARK></REMARK><REV_ORG_SOURCE_ID.RTK>PLANT</REV_ORG_SOURCE_ID.RTK><REV_ORG_SOURCE_CODE>980</REV_ORG_SOURCE_CODE><PLANT_ADDRESS>四川峨眉山市车箭路2号</PLANT_ADDRESS><SOURCE_SUPPLIER_CODE>103438</SOURCE_SUPPLIER_CODE><INVOICE_SUPPLIER_CODE>103438</INVOICE_SUPPLIER_CODE><TAX_CODE></TAX_CODE><LINE><UDF023>1-1</UDF023><XSOURCE_SEQ>810258</XSOURCE_SEQ><ITEM_CODE>101002</ITEM_CODE><BUSINESS_QTY>100000</BUSINESS_QTY><BUSINESS_UNIT_CODE>KM</BUSINESS_UNIT_CODE><PRICE_QTY>100000</PRICE_QTY><PRICE_UNIT_CODE>KM</PRICE_UNIT_CODE><PRICE>27.1368</PRICE><PLAN_ARRIVAL_DATE>2022-11-03 09:29:52</PLAN_ARRIVAL_DATE><AMOUNT>2713680</AMOUNT></LINE><LINE><UDF023>1-1</UDF023><XSOURCE_SEQ>810258</XSOURCE_SEQ><ITEM_CODE>101002</ITEM_CODE><BUSINESS_QTY>100000</BUSINESS_QTY><BUSINESS_UNIT_CODE>KM</BUSINESS_UNIT_CODE><PRICE_QTY>100000</PRICE_QTY><PRICE_UNIT_CODE>KM</PRICE_UNIT_CODE><PRICE>27.1368</PRICE><PLAN_ARRIVAL_DATE>2022-11-03 09:29:52</PLAN_ARRIVAL_DATE><AMOUNT>2713680</AMOUNT></LINE></HEADER></BODY>]]>";
//		JSONObject jsonObject = ConvertUtil.xmltoJson(data);
//		String content = jsonObject.get("content",String.class);
//		String str = StringUtil.dealxmlHeader(content);
//		JSONObject body = ConvertUtil.xmltoJson(str).getJSONObject("BODY");
//		PoBody orderBody = body.toBean(PoBody.class);
//		List<PoHeader> header = orderBody.getHEADER();
//		for (PoHeader poHeader : header) {
//			System.out.println(poHeader.getXSOURCE_NO());
//			List<PoLine> line = poHeader.getLINE();
//			for (PoLine poLine : line) {
//				System.out.println(poLine.toString());
//			}
//			System.out.println();
//
//
//		}


	}
	@Test
	void trim(){
		String str = "3.2*2+4*6";
		List<List<String>> lists = saleOrderTaskService.splitCablen(str);
		for (List<String> list : lists) {
			System.out.println(list.get(0)+":"+list.get(1));
		}


	}
	@Test
	public List<List<String>>  splitCablen(String cabLen){
		List<List<String>> list = new ArrayList<>();
		List<String> chrlder = new ArrayList<>();
		String newcabLen = cabLen = cabLen.replaceAll(" ", "");
		int index = cabLen.indexOf("+");
		//判断index是否为0
		if (index!=-1) {
			String[] split = cabLen.split("\\+");
			for (String s : split) {
				String[] split1 = s.split("\\*");
				chrlder.add(split1[0]);
				chrlder.add(split1[1]);
			}
		}else {
			String[] split1 = newcabLen.split("\\*");
			chrlder.add(split1[0]);
			chrlder.add(split1[1]);
			list.add(chrlder);
		}
		return list;

	}


}
