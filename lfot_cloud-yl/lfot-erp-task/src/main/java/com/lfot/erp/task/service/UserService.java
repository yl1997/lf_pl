package com.lfot.erp.task.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfot.erp.task.mapper.UserMapper;
import com.lfot.erp.task.entity.User;
@Service
@RequiredArgsConstructor
public class UserService extends ServiceImpl<UserMapper, User> {

    /**
     * 使用 master_2 数据源
     * @return
     */
    @DS("master_2")
    public List<User> getxxx() {
        return this.list();
    }

}
