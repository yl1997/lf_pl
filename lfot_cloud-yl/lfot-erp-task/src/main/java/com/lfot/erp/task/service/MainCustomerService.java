package com.lfot.erp.task.service;


import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfot.erp.task.common.constant.Constants;
import com.lfot.erp.task.common.constant.SoapPostXmlTemplate;
import com.lfot.erp.task.common.excetion.RequestfailedException;
import com.lfot.erp.task.common.excetion.SoapErroException;
import com.lfot.erp.task.common.util.HttpSoapUtils;
import com.lfot.erp.task.entity.MaintCustomer;
import com.lfot.erp.task.entity.webservice.param.SoapErpComParam;
import com.lfot.erp.task.entity.webservice.response.CustomerBody;
import com.lfot.erp.task.entity.webservice.response.CustomerHeader;
import com.lfot.erp.task.entity.webservice.response.CustomerLine;
import com.lfot.erp.task.mapper.MaintCustomerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class MainCustomerService extends ServiceImpl<MaintCustomerMapper, MaintCustomer> {
    private Logger logger = LoggerFactory.getLogger(KfBillService.class);
    @Autowired
    private HttpSoapUtils httpSoapUtils;

    public void syncCustomer() throws RequestfailedException, SoapErroException {
        SoapErpComParam param = new SoapErpComParam();
        param.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
        // 测试数据 2021-11-09
        param.setTransactionDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        //param.setTransactionDate("2021-11-09");
        String postData = SoapPostXmlTemplate.getCustomer(param);
        JSONObject result = httpSoapUtils.postSoap(Constants.SOAP_URL, postData);
        if (!httpSoapUtils.postStatus(result)){
            String responseMessage = result.get("X_RESPONSE_MESSAGE", String.class);
            logger.info("执行获取erp获取客户信息失败,失败原因:{}",responseMessage);
            throw new SoapErroException(result.get("X_RESPONSE_MESSAGE",String.class));
        }
        CustomerBody customerBody = httpSoapUtils.getBanByResponsData(result, CustomerBody.class);
        if (customerBody==null){
            return;
        }
        List<CustomerHeader> header = customerBody.getHEADER();
        CustomerHeader customerHeader = header.get(0);
        List<CustomerLine> line = customerHeader.getLINE();
        if (CollectionUtils.isEmpty(line)){
            return;
        }
        //循环获取当天客户信息
        for (CustomerLine customerLine : line) {
            //判断客户代码是否已经存在 存在不操作，不存在就新增
            String customerCode = customerLine.getCUSTOMER_CODE();
            MaintCustomer splCustomer = this.getOne(new QueryWrapper<MaintCustomer>().eq("cCode", customerCode));
            if (splCustomer==null){
                MaintCustomer maintCustomer = new MaintCustomer();
                maintCustomer.setCtreeno("00120_");
                maintCustomer.setCcode(customerLine.getCUSTOMER_CODE());
                maintCustomer.setCname(customerLine.getCUSTOMER_NAME());
                maintCustomer.setCphone(customerLine.getPHONE_NUMBER());
                maintCustomer.setCcontactaddress(customerLine.getADDRESS());
                this.save(maintCustomer);
                continue;
            }else {
                //已存在判断客户名称是否一致
                if (!(splCustomer.getCname().equals(customerLine.getCUSTOMER_NAME()) && splCustomer.getCcontactaddress().equals(customerLine.getADDRESS()))){
                    splCustomer.setCname(customerLine.getCUSTOMER_NAME());
                    splCustomer.setCphone(customerLine.getPHONE_NUMBER());
                    splCustomer.setCcontactaddress(customerLine.getADDRESS());
                    this.updateById(splCustomer);
                }
            }


        }


    }
}
