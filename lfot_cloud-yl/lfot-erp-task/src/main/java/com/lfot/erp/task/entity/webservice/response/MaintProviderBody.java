package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;

import java.util.List;

@Data
public class MaintProviderBody {
    private List<MaintProviderHeader> HEADER;
}
