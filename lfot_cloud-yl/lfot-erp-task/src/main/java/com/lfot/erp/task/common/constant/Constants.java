package com.lfot.erp.task.common.constant;

/**
 * 公共配置
 */
public class Constants {
    /**
     * erp soap访问地址
     */
    public static final String SOAP_URL = "http://10.60.2.70:8020/webservices/SOAProvider/plsql/cux_soa_pub_service_pkg/?wsdl";

//    /**
//     * 组织代码
//     */
//    public static final String ORGANIZATION_CODE = "980";
//
//    /**
//     * 默认请求那个时间段的数据
//     */
//    public static final String LAST_UPDATE_DATE = "2021-01-01";
//
//     /**
//     * 外部系统接收单号 成功单据号不能重复使用
//     */
//    public static final String TRANSACTION_NUMBER = "E10PORECV";

    //执行soap 返回状态 S-成功,E-失败,U-异常
    public static final String POST_SUCCESS = "S";

    public static final String POST_ERRO = "E";
    public static final String POST_EXCEPTION = "U";
}
