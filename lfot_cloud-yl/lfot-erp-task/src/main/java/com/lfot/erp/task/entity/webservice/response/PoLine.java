package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PoLine {
    private String PRICE;
    private String BUSINESS_UNIT_CODE;
    private String XSOURCE_SEQ;
    private String ITEM_CODE;
    private String BUSINESS_QTY;
    private String UDF023;
    private String AMOUNT;
    private String PRICE_QTY;
    private String PRICE_UNIT_CODE;
    private String PLAN_ARRIVAL_DATE;
}
