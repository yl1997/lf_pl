package com.lfot.erp.task.handler;

import com.lfot.erp.task.common.excetion.RequestfailedException;
import com.lfot.erp.task.common.excetion.SoapErroException;
import com.lfot.erp.task.service.MainCustomerService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 获取客户人员信息
 */
@Component
@Slf4j
public class ErpCustomerJobbHandler {
    @Autowired
    private MainCustomerService customerService;
    @XxlJob("syncCustomer")
    public void syncCustomer() throws SoapErroException, RequestfailedException {
        //同步客户信息
        customerService.syncCustomer();
    }
}
