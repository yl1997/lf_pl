package com.lfot.erp.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "kfBill")
public class KfBill implements Serializable {
    @TableId(value = "ID", type = IdType.INPUT)
    private Integer id;

    @TableField(value = "lAssessor")
    private Boolean lassessor;

    @TableField(value = "iState")
    private Integer istate;

    @TableField(value = "ikfBillType")
    private Integer ikfbilltype;

    @TableField(value = "cMaterielTypeCode")
    private String cmaterieltypecode;

    @TableField(value = "cMaterielCode")
    private String cmaterielcode;

    @TableField(value = "dDateTime")
    private Date ddatetime;

    @TableField(value = "dDateTimeAudit")
    private Date ddatetimeaudit;

    @TableField(value = "cBillCode")
    private String cbillcode;

    @TableField(value = "cPurchaseBill")
    private String cpurchasebill;

    @TableField(value = "cProviderCode")
    private String cprovidercode;

    @TableField(value = "cReceiveBill")
    private String creceivebill;

    @TableField(value = "cSPLID")
    private String csplid;

    @TableField(value = "cProdTaskBill")
    private String cprodtaskbill;

    @TableField(value = "cStockCode")
    private String cstockcode;

    @TableField(value = "cPlaceCode")
    private String cplacecode;

    @TableField(value = "nMainNumber")
    private Double nmainnumber;

    @TableField(value = "cMainUnit")
    private String cmainunit;

    @TableField(value = "nSecondNumber")
    private Double nsecondnumber;

    @TableField(value = "cSecondUnit")
    private String csecondunit;

    @TableField(value = "nConvertNumber")
    private Double nconvertnumber;

    @TableField(value = "cConvertUnit")
    private String cconvertunit;

    @TableField(value = "cCustomerCode")
    private String ccustomercode;

    @TableField(value = "cManHandle")
    private String cmanhandle;

    @TableField(value = "cAssessor")
    private String cassessor;

    @TableField(value = "cBillMaker")
    private String cbillmaker;

    @TableField(value = "cKeeper")
    private String ckeeper;

    @TableField(value = "cTestID")
    private String ctestid;

    @TableField(value = "cProdID")
    private String cprodid;

    @TableField(value = "cMemo")
    private String cmemo;

}