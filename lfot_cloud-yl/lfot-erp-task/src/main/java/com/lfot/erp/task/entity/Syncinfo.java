package com.lfot.erp.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sync_info")
public class Syncinfo implements Serializable {

    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "type")
    private String type;

    @TableField(value = "identification_code")
    private String identificationCode;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value = "status")
    private String status;

    @TableField(value = "msg")
    private String msg;

    @TableField(value = "item_code")
    private String itemCode;

}
