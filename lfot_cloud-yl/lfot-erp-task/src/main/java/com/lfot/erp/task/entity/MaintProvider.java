package com.lfot.erp.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "maintProvider")
public class MaintProvider implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "ID",type = IdType.AUTO)
    /**
     * [id]
     */
    private Integer id;

    /**
     * [ctreeno]
     */
    @TableField(value = "cTreeNo")
    private String ctreeno;

    /**
     * [ccode]
     */
    @TableField(value = "cCode")
    private String ccode;

    /**
     * [cname]
     */
    @TableField(value = "cName")
    private String cname;

    /**
     * [cmaterieltypecode]
     */
    @TableField(value = "cMaterielTypeCode")
    private String cmaterieltypecode;

    /**
     * [cprovidertype]
     */
    @TableField(value = "cProviderType")
    private String cprovidertype;

    /**
     * [cphone]
     */
    @TableField(value = "cPhone")
    private String cphone;

    /**
     * [ccontactman]
     */
    @TableField(value = "cContactman")
    private String ccontactman;

    /**
     * [cfax]
     */
    @TableField(value = "cFax")
    private String cfax;

    /**
     * [cpostalcode]
     */
    @TableField(value = "cPostalCode")
    private String cpostalcode;

    /**
     * [ccontactaddress]
     */
    @TableField(value = "cContactAddress")
    private String ccontactaddress;

    /**
     * [carea]
     */
    @TableField(value = "cArea")
    private String carea;

    /**
     * [cbusiness]
     */
    @TableField(value = "cBusiness")
    private String cbusiness;

    /**
     * [caccountbank]
     */
    @TableField(value = "cAccountBank")
    private String caccountbank;

    /**
     * [cbanknumber]
     */
    @TableField(value = "cBankNumber")
    private String cbanknumber;

    /**
     * [ctaxnumber]
     */
    @TableField(value = "cTaxNumber")
    private String ctaxnumber;

    /**
     * [cmemo]
     */
    @TableField(value = "cMemo")
    private String cmemo;

    /**
     * [ledit]
     */
    @TableField(value = "ledit")
    private String ledit;

    /**
     * [password]
     */
    @TableField(value = "password")
    private String password;

}
