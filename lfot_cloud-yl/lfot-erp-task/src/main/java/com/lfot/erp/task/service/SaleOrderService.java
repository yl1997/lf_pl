package com.lfot.erp.task.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfot.erp.task.common.constant.Constants;
import com.lfot.erp.task.common.constant.SoapPostXmlTemplate;
import com.lfot.erp.task.common.excetion.RequestfailedException;
import com.lfot.erp.task.common.excetion.SoapErroException;
import com.lfot.erp.task.common.util.*;
import com.lfot.erp.task.entity.SaleOrderDetail;
import com.lfot.erp.task.entity.SaleOrderMain;
import com.lfot.erp.task.entity.webservice.param.SoapErpComParam;
import com.lfot.erp.task.entity.webservice.response.SaleOrderBody;
import com.lfot.erp.task.entity.webservice.response.SaleOrderHeader;
import com.lfot.erp.task.entity.webservice.response.SaleOrderLine;
import com.lfot.erp.task.mapper.SaleOrderMapper;
import com.microsoft.sqlserver.jdbc.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SaleOrderService extends ServiceImpl<SaleOrderMapper,SaleOrderMain> {
    private Logger logger = LoggerFactory.getLogger(KfBillService.class);
    @Autowired
    private HttpSoapUtils httpSoapUtils;

    @Autowired
    private SaleOrderDetailService saleOrderDetailService;

    /**
     *获取销售订单
     */
    @Transactional
    public void syncSaleOrder() throws RequestfailedException, SoapErroException, ParseException {
        SoapErpComParam param = new SoapErpComParam();
        param.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
        param.setTransactionDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        //param.setTransactionDate("2022-11-07");
        String postData = SoapPostXmlTemplate.saleOrder(param);
        JSONObject result = httpSoapUtils.postSoap(Constants.SOAP_URL, postData);
        if (!httpSoapUtils.postStatus(result)){
            String responseMessage = result.get("X_RESPONSE_MESSAGE", String.class);
            logger.info("执行获取erp获取销售订单失败,失败原因:{}",responseMessage);
            throw new SoapErroException(result.get("X_RESPONSE_MESSAGE",String.class));
        }
        SaleOrderBody body = httpSoapUtils.getBanByResponsData(result, SaleOrderBody.class);
        if (body==null){
            return;
        }
        List<SaleOrderHeader> header = body.getHEADER();
        for (SaleOrderHeader saleOrderHeader : header) {
                //根据当前订单查询当前销售订单是否存在
                SaleOrderMain saleOrderDo = this.getOne(new QueryWrapper<SaleOrderMain>()
                        .eq("cOrderBH", saleOrderHeader.getXSOURCE_NO()));
                //判度是否存在 存在不新增 不存在直接新增
                if (saleOrderDo!=null) {
                    //修改操作
                    this.updateSaleOrder(saleOrderHeader,saleOrderDo);
                }else {
                    /**
                     * 新增销售订单主表信息
                     */
                    SaleOrderMain saleOrderMain = new SaleOrderMain();
                    //0未审核
                    saleOrderMain.setLassessor((byte) 0);
                    //求销售订单总量
                    saleOrderMain.setNsumlength(CollectionUtils.isEmpty(saleOrderHeader.getLINE()) ? new BigDecimal(0)
                            : BigDecimalUtil.div(saleOrderHeader.getLINE().stream().
                            mapToDouble(SaleOrderLine::getBUSINESS_QTY).sum(),1000,4));
                    //设置状态
                    saleOrderMain.setCstatecode("8800");
                    //唯一标识
                    saleOrderMain.setCbillcode("SA"+ DateUtilS.customFormatDate("yyyyMMddHHmmss")+ NumberUtils.generateCode(3));
                    //订单号
                    saleOrderMain.setCorderbh(saleOrderHeader.getXSOURCE_NO());
                    saleOrderMain.setCorderbhrelation(saleOrderHeader.getXSOURCE_NO());
                    //订单erp时间
                    saleOrderMain.setDdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(saleOrderHeader.getLAST_UPDATE_DATE()));
                    //客户代码
                    saleOrderMain.setCcustomercode(saleOrderHeader.getCUSTOMER_CODE());
                    //销售员人名
                    saleOrderMain.setCsalesman(StringUtils.isEmpty(saleOrderHeader.getEMPLOYEE_CODEL())?"":saleOrderHeader.getEMPLOYEE_CODEL());
                    //员工号  saleOrderHeader.getEMPLOYEE_CODEL()
                    saleOrderMain.setCsalesno(StringUtils.isEmpty(saleOrderHeader.getEMPLOYEE_CODEL())?"":saleOrderHeader.getEMPLOYEE_CODEL());
                    //设置 订单类型
                    saleOrderMain.setCOrderTypeCode(saleOrderHeader.getORDER_CATEGORY_CODE());
                    boolean saveResult = this.save(saleOrderMain);
                    /**
                     * 新增子表信息
                     */
                    if (saveResult){
                        saveSaleOrderDetail(saleOrderHeader.getLINE(),saleOrderMain);
                    }
                }
        }
        return;
    }

    /**
     * 获取段长和数量
     * @param cabLen
     * @return
     */
    public List<List<String>>  splitCablen(String cabLen){
        List<List<String>> list = new ArrayList<>();
         String newcabLen = cabLen = cabLen.replaceAll(" ", "");
         if (StringUtils.isEmpty(newcabLen)){
             return null;
         }
         int index = cabLen.indexOf("+");
        //判断index是否为0
        if (index!=-1) {
            String[] split = cabLen.split("\\+");
            for (String s : split) {
                ArrayList<String> chrlder = new ArrayList<>();
                String[] split1 = s.split("\\*");
                chrlder.add(split1[0]);
                chrlder.add(split1[1]);
                list.add(chrlder);
            }
        }else {
            ArrayList<String> chrlder = new ArrayList<>();
            String[] split1 = newcabLen.split("\\*");
            chrlder.add(split1[0]);
            chrlder.add(split1[1]);
            list.add(chrlder);
        }
        return list;
        }

    /**
     * 保存销售子表信息
     * @param line
     * @param saleOrderMain
     */
    public void saveSaleOrderDetail(List<SaleOrderLine> line,SaleOrderMain saleOrderMain ){
        for (SaleOrderLine saleOrderLine : line) {
            SaleOrderDetail saleOrderDetail = new SaleOrderDetail();
            //规则号
            saleOrderDetail.setCbillcode(saleOrderMain.getCbillcode());
            //物料代码
            saleOrderDetail.setCmaterielcode(saleOrderLine.getITEM_CODE());
            //设置行号
            saleOrderDetail.setCLineNo(saleOrderLine.getLineNumber());
            //获取每个物料对应的段长  3.0000*5+4.000*2 saleOrderLine.getCAB_LEN()
            List<List<String>> cabLens = splitCablen(saleOrderLine.getCAB_LEN());
            if (CollectionUtils.isEmpty(cabLens)){
                return;
            }
            for (List<String> cabLen : cabLens) {
                //获取段长和数量
                String segmentLength  = cabLen.get(0);
                //数量
                String num = cabLen.get(1);
                //求总和
                BigDecimal segmentSum = BigDecimalUtil.mul(Double.valueOf(segmentLength), Double.valueOf(num));
                saleOrderDetail.setNorderlength(segmentSum);
                saleOrderDetail.setNdurmlength(new BigDecimal(segmentLength));
                saleOrderDetailService.save(saleOrderDetail);
            }
        }
    }

    public void updateSaleOrder(SaleOrderHeader saleOrderHeader, SaleOrderMain saleOrderDo) throws ParseException {
        //判断是否是已审核 如果是已审核的,就不等在新增
        if (saleOrderDo.getLassessor() == 1){
            return;
         }
        //在判断现在订单修改时间和保存的订单时间是否一致
        String erpupdateDate = saleOrderHeader.getLAST_UPDATE_DATE();
        Date saleOrderDoTime = saleOrderDo.getDdatetime();
        //如果时间一致证明之前订单未修改
        if (DateUtil.isSameTime(saleOrderDoTime, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(erpupdateDate))) {
            return;
        }
        saleOrderDo.setNsumlength(CollectionUtils.isEmpty(saleOrderHeader.getLINE()) ? new BigDecimal(0)
                : BigDecimalUtil.div(saleOrderHeader.getLINE().stream().
                mapToDouble(SaleOrderLine::getBUSINESS_QTY).sum(),1000,4));
        saleOrderDo.setCcustomercode(saleOrderHeader.getCUSTOMER_CODE());
        saleOrderDo.setCsalesno(saleOrderHeader.getEMPLOYEE_CODEL());
        //修改子表信息
        boolean update = this.updateById(saleOrderDo);
        if (update){
            //直接根据物料代码将之前录入的订单没有审核的，直接删除重新生成段长和数量
            saleOrderDetailService.remove(new QueryWrapper<SaleOrderDetail>().eq("cBillCode", saleOrderDo.getCbillcode()));
            saveSaleOrderDetail(saleOrderHeader.getLINE(),saleOrderDo);
        }

    }


}
