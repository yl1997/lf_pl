package com.lfot.erp.task.service;

import cn.hutool.json.JSONObject;
import com.lfot.erp.task.common.constant.Constants;
import com.lfot.erp.task.common.constant.SoapPostXmlTemplate;
import com.lfot.erp.task.common.enums.TransactionTypeEnum;
import com.lfot.erp.task.common.excetion.RequestfailedException;
import com.lfot.erp.task.common.excetion.SoapErroException;
import com.lfot.erp.task.common.util.DateUtilS;
import com.lfot.erp.task.common.util.HttpSoapUtils;
import com.lfot.erp.task.entity.DTO.FyHistoryRecordDto;
import com.lfot.erp.task.entity.webservice.param.SoapErpComParam;
import com.lfot.erp.task.entity.webservice.response.*;
import com.lfot.erp.task.mapper.FyHistoryRecordMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 货物发运 service
 */
@Service
public class FyBillService {

    private Logger logger = LoggerFactory.getLogger(KfBillService.class);

    @Autowired
    private HttpSoapUtils httpSoapUtils;

    @Autowired
    private FyHistoryRecordMapper fyHistoryRecordMapper;

    @Autowired
    private SyncRunnable syncRunnable;

    public void syncfyBill() throws Exception {
        //1.查询当天需要同步的发运数据
        List<FyHistoryRecordDto> fyHistoryRecordDtos = fyHistoryRecordMapper.qureyFyBillNumByDate(DateUtilS.getDayBegin(), DateUtilS.getDayEnd());
        if (CollectionUtils.isEmpty(fyHistoryRecordDtos)){
            return;
        }
        //查询远程erp的订单库
        SoapErpComParam param = new SoapErpComParam();
        //生成setBatchId，自增主键，每次请求不一样。
        param.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
        param.setTransactionDate("2021-01-01");
        JSONObject result = httpSoapUtils.postSoap(Constants.SOAP_URL, SoapPostXmlTemplate.saleOrder(param));
        //判断是否请求成功
        if (!httpSoapUtils.postStatus(result)){
            String responseMessage = result.get("X_RESPONSE_MESSAGE", String.class);
            logger.info("执行获取erp退库访问失败,失败原因:{}",responseMessage);
            throw new SoapErroException(result.get("X_RESPONSE_MESSAGE",String.class));
        }
        SaleOrderBody body = httpSoapUtils.getBanByResponsData(result, SaleOrderBody.class);
        if (body==null){
            return;
        }
        List<SaleOrderHeader> header = body.getHEADER();
       //重新进行收集 key:订单   value:无量编号和数量list
        Map<String, List<SaleOrderLine>> collect = header.stream().collect(Collectors.toMap(SaleOrderHeader::getXSOURCE_NO, SaleOrderHeader::getLINE));
        List<FyHistoryRecordDto>  list = createsoapParam(fyHistoryRecordDtos, collect);
        for (FyHistoryRecordDto fyHistoryRecordDto : list) {
            SoapErpComParam cPurchaseBillparam = new SoapErpComParam();
            cPurchaseBillparam.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
            cPurchaseBillparam.setTransactionType(TransactionTypeEnum.CANCELLINT.getCode());
            cPurchaseBillparam.setTransationNumber(Calendar.getInstance().getTimeInMillis() + "");
            String postData = SoapPostXmlTemplate.getfyBill(cPurchaseBillparam, fyHistoryRecordDto);
            syncRunnable.syncPost(Constants.SOAP_URL,postData,httpSoapUtils,TransactionTypeEnum.SHIPMENT.getCode(), fyHistoryRecordDto.getCDeliveryBH(),fyHistoryRecordDto.getCMaterielCode());

        }

    }

    private List<FyHistoryRecordDto> createsoapParam(List<FyHistoryRecordDto> fyHistoryRecordDtos, Map<String, List<SaleOrderLine>> collect) {
        for (FyHistoryRecordDto fyHistoryRecordDto : fyHistoryRecordDtos) {
            //获取销售订单
            String cDeliveryBH = fyHistoryRecordDto.getCDeliveryBH();
            //根据erp销售订单获取map的采购订单对应的物料代码
            List<SaleOrderLine> saleOrderLines = collect.get(cDeliveryBH);
            //获取spl销售订单对应的物料代码获取行号
            String cMaterielCode = fyHistoryRecordDto.getCMaterielCode();
            String linecode = this.lineCode(cMaterielCode, saleOrderLines);
            if (StringUtils.isEmpty(linecode)){
                continue;
            }
            fyHistoryRecordDto.setCDeliveryBH(linecode);
        }
        return fyHistoryRecordDtos;

    }

    private String lineCode(String cMaterielCode, List<SaleOrderLine> saleOrderLines) {
        for (SaleOrderLine saleOrderLine : saleOrderLines) {
            if (cMaterielCode.equals(saleOrderLine.getITEM_CODE())){
                return saleOrderLine.getLineNumber();
            }

        }
        return null;
    }

}
