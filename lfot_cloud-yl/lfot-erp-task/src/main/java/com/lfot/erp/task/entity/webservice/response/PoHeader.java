package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;
import lombok.ToString;

import java.util.List;
@Data
@ToString
public class PoHeader {
//    private String PURCHASE_TYPE;
//    private String REV_ORG_SOURCE_CODE;
//    private String PLANT_ADDRESS;
//    private String EMPLOYEE_CODE;
//    private String CURRENCY_CODE;
//    private String SUPPLIER_ADDR_NAME;
//    private String INVOICE_SUPPLIER_CODE;
    //只需要订单号
    private String XSOURCE_NO;
//    private String SUPPLIER_CONTACT_NAME;
//    private String TRANSACTION_ID;
//    private String PURCHASE_DATE;
//    private String REMARK;
//    private String TAX_CODE;
//    private String SUPPLIER_CODE;
//    private String DOC_DATE;
//    private String SOURCE_SUPPLIER_CODE;
    private List<PoLine> LINE;
}
