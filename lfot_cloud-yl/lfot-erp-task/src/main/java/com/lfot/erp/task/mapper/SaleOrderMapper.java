package com.lfot.erp.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfot.erp.task.entity.SaleOrderMain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SaleOrderMapper extends BaseMapper<SaleOrderMain> {

}
