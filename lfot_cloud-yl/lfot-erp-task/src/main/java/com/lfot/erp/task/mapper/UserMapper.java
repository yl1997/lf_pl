package com.lfot.erp.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfot.erp.task.entity.KfBill;
import com.lfot.erp.task.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

}