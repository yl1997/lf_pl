package com.lfot.erp.task.service;

import cn.hutool.json.JSONObject;
import com.lfot.erp.task.common.constant.Constants;
import com.lfot.erp.task.common.excetion.RequestfailedException;
import com.lfot.erp.task.common.util.HttpSoapUtils;
import com.lfot.erp.task.entity.Syncinfo;
import com.lfot.erp.task.mapper.SyncinfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SyncRunnable {
    @Autowired
    private SyncinfoMapper syncinfoMapper;
    private Logger logger = LoggerFactory.getLogger(SyncRunnable.class);
    /**
     * 异步任务进行同步请求
     * @param url
     * @param xmlData
     * @param itemCode 物料编号
     */
    public JSONObject syncPost(String url, String xmlData, HttpSoapUtils httpSoapUtils, String transactionType,String cPurchaseBill,String itemCode) throws Exception {
        JSONObject result = httpSoapUtils.postSoap(url, xmlData);
        //判断是否请求成功
        logger.info("执行{}成功:订单号:{}",transactionType,cPurchaseBill);
        Syncinfo syncinfo = new Syncinfo();
        syncinfo.setCreateTime(new Date());
        //操作类型
        syncinfo.setItemCode(itemCode);
        syncinfo.setStatus(httpSoapUtils.postStatus(result)?Constants.POST_SUCCESS:Constants.POST_ERRO);
        syncinfo.setType(transactionType);
        syncinfo.setIdentificationCode(cPurchaseBill);
        syncinfo.setMsg(result.get("X_RESPONSE_MESSAGE", String.class));
        syncinfoMapper.insert(syncinfo);
        return result;
    }

}
