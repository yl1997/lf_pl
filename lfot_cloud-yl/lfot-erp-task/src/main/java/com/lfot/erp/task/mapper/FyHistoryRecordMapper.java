package com.lfot.erp.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfot.erp.task.entity.DTO.FyHistoryRecordDto;
import com.lfot.erp.task.entity.Fyhistoryrecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface FyHistoryRecordMapper extends BaseMapper<Fyhistoryrecord> {

    /**
     * 查询发运数据
     * @return
     */
    List<FyHistoryRecordDto> qureyFyBillNumByDate(@Param("startTime") Date startTime, @Param("endTime") Date endTime);

    /**
     * 通过时间查询扫码出库数据
     * @return
     */
    List<FyHistoryRecordDto> qureyPickUpStoreByDate(@Param("startTime") Date startTime, @Param("endTime") Date endTime);
}
