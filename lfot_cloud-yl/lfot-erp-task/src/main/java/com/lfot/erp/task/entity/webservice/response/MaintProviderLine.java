package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;

@Data
public class MaintProviderLine {
    private String CF_COMPANY_CODE;
    private String SUPPLIER_NAME;
    private String SUPPLIER_CODE;
    private String CURRENCY_CODE;
    private String P_EMPLOYEE_CODE;
    private String ADDRESS;
    private String POSTCODEp;
    private String CS_TAX_CLASSIFICATION_CODE;
    private String FAX;
    private String CONTACT;
    private String TELEPHONE;
    private String EMAIL;
    private String Vat_Registration_Num;
    private String Term_Name;
    private String LAST_UPDATE_DATE;
}
