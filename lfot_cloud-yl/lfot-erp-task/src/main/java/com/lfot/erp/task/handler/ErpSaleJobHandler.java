package com.lfot.erp.task.handler;

import com.lfot.erp.task.service.FyhistoryrecordService;
import com.lfot.erp.task.service.SaleOrderService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ERP 销售处理器
 */
@Component

@Slf4j
public class ErpSaleJobHandler {
    @Autowired
    private SaleOrderService saleOrderDetailService;
    @Autowired
    private FyhistoryrecordService fyhistoryrecordService;

    /**
     * erp销售订单tospl
     * @throws Exception
     */
    @XxlJob("SaleOrderFromErpTospl")
    public void syncSaleOrder() throws Exception {
        saleOrderDetailService.syncSaleOrder();
    }

    /**
     * 同步挑库
     * @throws Exception
     */
    @XxlJob("syncPickUpstore")
    public void syncPickUpstore() throws Exception {
        fyhistoryrecordService.pickUpstore();
    }
}
