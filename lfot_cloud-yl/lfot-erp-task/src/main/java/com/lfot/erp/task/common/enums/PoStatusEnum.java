package com.lfot.erp.task.common.enums;

/**
 * 采购状态 入库 出库
 */
public enum PoStatusEnum {
    CANCELLINT(1018,"退库");
    private int code;
    private String msg;

    PoStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
