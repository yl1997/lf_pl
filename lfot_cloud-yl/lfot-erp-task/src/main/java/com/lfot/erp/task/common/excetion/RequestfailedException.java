package com.lfot.erp.task.common.excetion;

public class RequestfailedException extends Exception{
    public RequestfailedException(String msg) {
        super(msg);
    }
}
