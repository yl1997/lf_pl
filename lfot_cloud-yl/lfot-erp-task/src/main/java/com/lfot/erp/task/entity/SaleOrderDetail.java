package com.lfot.erp.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "SaleOrderDetail")
public class SaleOrderDetail  implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "ID",type = IdType.AUTO)
    /**
     * [id]
     */
    private Integer id;

    /**
     * [cbillcode]
     */
    @TableField(value = "cBillCode")
    private String cbillcode;

    /**
     * [lprodaccept]
     */
    @TableField(value = "lProdAccept")
    private Byte lprodaccept;

    /**
     * [cmaterielcode]
     */
    @TableField(value = "cMaterielCode")
    private String cmaterielcode;

    /**
     * [nfiberxs]
     */
    @TableField(value = "nFiberXS")
    private Integer nfiberxs;

    /**
     * [norderlength]
     */
    @TableField(value = "nOrderLength")
    private BigDecimal norderlength;

    /**
     * [ndurmlength]
     */
    @TableField(value = "nDurmLength")
    private BigDecimal ndurmlength;

    /**
     * [cprodcode]
     */
    @TableField(value = "cProdCode")
    private String cprodcode;

    /**
     * [fprice]
     */
    @TableField(value = "fPrice")
    private BigDecimal fprice;

    /**
     * [cprojectname]
     */
    @TableField(value = "cProjectName")
    private String cprojectname;

    /**
     * [cprintinformation]
     */
    @TableField(value = "cPrintInformation")
    private String cprintinformation;

    /**
     * [nstockdrums]
     */
    @TableField(value = "nStockDrums")
    private Integer nstockdrums;

    /**
     * [ndeliverydrums]
     */
    @TableField(value = "nDeliveryDrums")
    private Integer ndeliverydrums;

    /**
     * [ctaskcode]
     */
    @TableField(value = "cTaskCode")
    private String ctaskcode;

    /**
     * [cmemo]
     */
    @TableField(value = "cMemo")
    private String cmemo;

    /**
     * [cmemo2]
     */
    @TableField(value = "cMemo2")
    private String cmemo2;

    /**
     * [cprodmemo]
     */
    @TableField(value = "cProdMemo")
    private String cprodmemo;

    /**
     * [cfibertypecode]
     */
    @TableField(value = "cFiberTypeCode")
    private String cfibertypecode;

    /**
     * [creportcode]
     */
    @TableField(value = "cReportCode")
    private String creportcode;

    /**
     * [nreportcount]
     */
    @TableField(value = "nReportCount")
    private Integer nreportcount;

    /**
     * [ccolortablecodefiber]
     */
    @TableField(value = "cColorTableCodeFiber")
    private String ccolortablecodefiber;

    /**
     * [ccolortablecodetube]
     */
    @TableField(value = "cColorTableCodeTube")
    private String ccolortablecodetube;

    /**
     * [lselect]
     */
    @TableField(value = "lSelect")
    private Byte lselect;

    /**
     * [cattcode]
     */
    @TableField(value = "cAttCode")
    private String cattcode;

    /**
     * [lcoverfilm]
     */
    @TableField(value = "lCoverFilm")
    private Byte lcoverfilm;

    /**
     * [lplasticpackagehit]
     */
    @TableField(value = "lPlasticPackageHit")
    private Byte lplasticpackagehit;

    /**
     * [cplasticpackagehitmemo]
     */
    @TableField(value = "cPlasticPackageHitMemo")
    private String cplasticpackagehitmemo;

    /**
     * [cdrumtype]
     */
    @TableField(value = "cDrumType")
    private String cdrumtype;

    /**
     * [cdrummaterial]
     */
    @TableField(value = "cDrumMaterial")
    private String cdrummaterial;

    /**
     * [cprodrequirement]
     */
    @TableField(value = "cProdRequirement")
    private String cprodrequirement;

    /**
     * [cprintaddmemo]
     */
    @TableField(value = "cPrintAddMemo")
    private String cprintaddmemo;

    /**
     * [cprinttype]
     */
    @TableField(value = "cPrintType")
    private String cprinttype;

    @TableField(value = "cLineNo")
    private String cLineNo;


}
