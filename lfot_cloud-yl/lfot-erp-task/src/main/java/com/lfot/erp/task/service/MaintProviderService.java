package com.lfot.erp.task.service;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfot.erp.task.common.constant.Constants;
import com.lfot.erp.task.common.constant.SoapPostXmlTemplate;
import com.lfot.erp.task.common.excetion.RequestfailedException;
import com.lfot.erp.task.common.excetion.SoapErroException;
import com.lfot.erp.task.common.util.HttpSoapUtils;
import com.lfot.erp.task.entity.MaintCustomer;
import com.lfot.erp.task.entity.MaintProvider;
import com.lfot.erp.task.entity.webservice.param.SoapErpComParam;
import com.lfot.erp.task.entity.webservice.response.*;
import com.lfot.erp.task.mapper.MaintProviderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class MaintProviderService extends ServiceImpl<MaintProviderMapper, MaintProvider> {
    private Logger logger = LoggerFactory.getLogger(KfBillService.class);
    @Autowired
    private HttpSoapUtils httpSoapUtils;
    public void syncMainProvider() throws RequestfailedException, SoapErroException {
        SoapErpComParam param = new SoapErpComParam();
        param.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
        param.setTransactionDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        //param.setTransactionDate("2020-11-09");
        String postData = SoapPostXmlTemplate.getMainProvider(param);
        JSONObject result = httpSoapUtils.postSoap(Constants.SOAP_URL, postData);
        if (!httpSoapUtils.postStatus(result)){
            String responseMessage = result.get("X_RESPONSE_MESSAGE", String.class);
            logger.info("执行获取erp获取供应商信息失败,失败原因:{}",responseMessage);
            throw new SoapErroException(result.get("X_RESPONSE_MESSAGE",String.class));
        }
        MaintProviderBody maintProviderBody = httpSoapUtils.getBanByResponsData(result, MaintProviderBody.class);
        if (maintProviderBody==null){
            return;
        }
        List<MaintProviderHeader> header = maintProviderBody.getHEADER();
        MaintProviderHeader maintProviderHeader = header.get(0);
        List<MaintProviderLine> line = maintProviderHeader.getLINE();
        if (CollectionUtils.isEmpty(line)){
            return;
        }
        //循环获取当天客户信息
        for (MaintProviderLine maintProviderLine : line) {
            MaintProvider resultPro = this.getOne(new QueryWrapper<MaintProvider>().eq("cCode", maintProviderLine.getSUPPLIER_CODE()));
            if (resultPro!=null){
                //判断名称是否一致
                if(!resultPro.getCname().equals(maintProviderLine.getSUPPLIER_NAME())){
                    resultPro.setCname(maintProviderLine.getSUPPLIER_NAME());
                    resultPro.setCcontactaddress(maintProviderLine.getADDRESS());
                    resultPro.setCphone(maintProviderLine.getTELEPHONE());
                    resultPro.setCcontactman(maintProviderLine.getCONTACT());
                    this.updateById(resultPro);
                    continue;
                }
            }
            //工厂编号
            String companycode = maintProviderLine.getCF_COMPANY_CODE();
            //供应商名称
            String suppliername = maintProviderLine.getSUPPLIER_NAME();
            //供应商编号
            String suppliercode = maintProviderLine.getSUPPLIER_CODE();
            //地址
            String address = maintProviderLine.getADDRESS();
            //电话
            String telephone = maintProviderLine.getTELEPHONE();
            //联系人
            String contact = maintProviderLine.getCONTACT();
            MaintProvider maintProvider = new MaintProvider();
            maintProvider.setCtreeno(companycode);
            maintProvider.setCname(suppliername);
            maintProvider.setCcode(suppliercode);
            maintProvider.setCcontactaddress(address);
            maintProvider.setCphone(telephone);
            maintProvider.setCcontactman(contact);
            this.save(maintProvider);
            }
        }
    }

