package com.lfot.erp.task.common.excetion;

public class SoapErroException extends Exception{

    public SoapErroException(String msg) {
        super(msg);
    }
}
