package com.lfot.erp.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "SaleOrderMain")
public class SaleOrderMain implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "ID",type = IdType.AUTO)
    private Integer id;

    /**
     * [lassessor]
     */
    @TableField(value = "lAssessor")
    private Byte lassessor;

    /**
     * [cstatecode]
     */
    @TableField(value = "cStateCode")
    private String cstatecode;

    /**
     * [cbillcode]
     */
    @TableField(value = "cBillCode")
    private String cbillcode;

    /**
     * [corderbh]
     */
    @TableField(value = "cOrderBH")
    private String corderbh;

    /**
     * [corderbhrelation]
     */
    @TableField(value = "cOrderBHRelation")
    private String corderbhrelation;

    /**
     * [cbzcode]
     */
    @TableField(value = "cBZCode")
    private String cbzcode;

    /**
     * [cmop]
     */
    @TableField(value = "cMOP")
    private String cmop;

    /**
     * [ddatetime]
     */
    @TableField(value = "dDatetime")
    private Date ddatetime;

    /**
     * [csalesman]
     */
    @TableField(value = "cSalesman")
    private String csalesman;

    /**
     * [cordertype]
     */
    @TableField(value = "cOrderType")
    private String cordertype;

    /**
     * [ccustomercode]
     */
    @TableField(value = "cCustomerCode")
    private String ccustomercode;

    /**
     * [clinkman]
     */
    @TableField(value = "cLinkman")
    private String clinkman;

    /**
     * [clinkphone]
     */
    @TableField(value = "cLinkPhone")
    private String clinkphone;

    /**
     * [nsumlength]
     */
    @TableField(value = "nSumLength")
    private BigDecimal nsumlength;

    /**
     * [noutfactorylength]
     */
    @TableField(value = "nOutFactoryLength")
    private BigDecimal noutfactorylength;

    /**
     * [ntruckcodelength]
     */
    @TableField(value = "nTruckCodeLength")
    private BigDecimal ntruckcodelength;

    /**
     * [ntruckcodelength_no]
     */
    @TableField(value = "nTruckCodeLength_NO")
    private BigDecimal ntruckcodelengthNo;

    /**
     * [nxskm]
     */
    @TableField(value = "nXSKM")
    private BigDecimal nxskm;

    /**
     * [fsumprice]
     */
    @TableField(value = "fSumPrice")
    private BigDecimal fsumprice;

    /**
     * [nlengthdeviation]
     */
    @TableField(value = "nLengthDeviation")
    private Float nlengthdeviation;

    /**
     * [cpaymentmode]
     */
    @TableField(value = "cPaymentMode")
    private String cpaymentmode;

    /**
     * [ddatetimedelivery]
     */
    @TableField(value = "dDatetimeDelivery")
    private Date ddatetimedelivery;

    /**
     * [creceiverman]
     */
    @TableField(value = "cReceiverMan")
    private String creceiverman;

    /**
     * [cphone]
     */
    @TableField(value = "cPhone")
    private String cphone;

    /**
     * [cassessor]
     */
    @TableField(value = "cAssessor")
    private String cassessor;

    /**
     * [cbillmaker]
     */
    @TableField(value = "cBillMaker")
    private String cbillmaker;

    /**
     * [cmemo]
     */
    @TableField(value = "cMemo")
    private String cmemo;

    /**
     * [cfiberprovidercode]
     */
    @TableField(value = "cFiberProviderCode")
    private String cfiberprovidercode;

    /**
     * [cdurmmemo]
     */
    @TableField(value = "cDurmMemo")
    private String cdurmmemo;

    /**
     * [cmemo2]
     */
    @TableField(value = "cMemo2")
    private String cmemo2;

    /**
     * [csalesno]
     */
    @TableField(value = "cSalesNO")
    private String csalesno;

    /**
     * [corderpo]
     */
    @TableField(value = "cOrderPO")
    private String corderpo;

    /**
     * [cmemosubst]
     */
    @TableField(value = "cMemoSubst")
    private String cmemosubst;

    /**
     * [ddatetimesys]
     */
    @TableField(value = "dDatetimeSYS")
    private Date ddatetimesys;

    @TableField(value = "cOrderTypeCode")
    private String cOrderTypeCode;


}
