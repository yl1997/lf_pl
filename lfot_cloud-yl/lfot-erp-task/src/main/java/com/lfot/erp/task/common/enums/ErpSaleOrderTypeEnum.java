package com.lfot.erp.task.common.enums;


public enum ErpSaleOrderTypeEnum {

    ORDER("ORDER","销售订单"),
    RETURN("RETURN","退货订单");

    private String code;
    private String msg;

    ErpSaleOrderTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
