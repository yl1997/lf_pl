package com.lfot.erp.task.common.util;

import cn.hutool.json.JSONObject;
import com.lfot.erp.task.common.constant.Constants;
import com.lfot.erp.task.common.excetion.RequestfailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * @Auther: 1770606609@qq.com
 * @Date: 2022/10/26
 * @Description: com.example.util
 * @Version: 1.0
 */

@Component
public class HttpSoapUtils {

    @Autowired
    private RestTemplate restTemplate;

    private Logger logger = LoggerFactory.getLogger(HttpSoapUtils.class);
    /**
     * 执行soap请求,并将相应结果转成map
     * @param url
     * @param dataXml
     * @return
     */
    public  JSONObject postSoap(String url, String dataXml) throws RequestfailedException {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("text/xml;charset=UTF-8");
        headers.setContentType(type);
        HttpEntity<String> request = new HttpEntity<String>(dataXml + "", headers);
        String response = null;
        try {
            response = restTemplate.postForObject(url, request, String.class);
        } catch (Exception e) {
            logger.error("执行soap{}远程调用失败，异常信息：{}",url,e.getMessage());
            throw new RequestfailedException(e.getMessage());
        }
        JSONObject jsonObject = ConvertUtil.xmltoJson(response);
        JSONObject outputParameters = jsonObject.getJSONObject("env:Envelope").getJSONObject("env:Body").getJSONObject("OutputParameters");
        return outputParameters;
        }

        //判断请求是否成功
        public  boolean postStatus(JSONObject result){
            if (Constants.POST_ERRO.equals(result.get("X_RESPONSE_STATUS",String.class)) ||
                    Constants.POST_EXCEPTION.equals(result.get("X_RESPONSE_STATUS",String.class))){
                return false;
            }
            return true;
        }

    /**
     * erp远程嗲用获取data数据
     * @param result
     * @param classs
     * @param <T>
     * @return
     */
    public <T> T getBanByResponsData(JSONObject result, Class<T> classs){
            String responseData = result.get("X_RESPONSE_DATA", String.class);
            String content = ConvertUtil.xmltoJson(responseData).get("content",String.class);
            String str = StringUtil.dealxmlHeader(content);
            JSONObject body = null;
            try {
                 body = ConvertUtil.xmltoJson(str).getJSONObject("BODY");
            }catch (Exception e){
                return null;
            }
            return body.toBean(classs);
        }

}
