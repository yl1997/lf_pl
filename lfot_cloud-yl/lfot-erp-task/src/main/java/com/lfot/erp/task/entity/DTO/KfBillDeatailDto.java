package com.lfot.erp.task.entity.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class KfBillDeatailDto {
    /**
     * 采购物料编号
     */
    private String cMaterielCode;

    /**
     * 数量
     */
    private BigDecimal num;

    /**
     * 物料编号对应的行号
     */
    private String lineNum;
}
