package com.lfot.erp.task.handler;

import com.lfot.erp.task.common.excetion.RequestfailedException;
import com.lfot.erp.task.common.excetion.SoapErroException;
import com.lfot.erp.task.service.MaintProviderService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MaintProviderJobHandler {
    @Autowired
    private MaintProviderService maintProviderService;
    @XxlJob("syncMainProtoSpl")
    public void syncMainProtoSpl() throws SoapErroException, RequestfailedException {
        maintProviderService.syncMainProvider();;
    }
}
