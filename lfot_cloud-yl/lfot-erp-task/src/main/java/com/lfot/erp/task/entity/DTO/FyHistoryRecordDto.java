package com.lfot.erp.task.entity.DTO;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 发运同步erp的数据
 */
@Data
@ToString
public class FyHistoryRecordDto {

    private String cDeliveryBH;

    /**
     * 合并后的订单号
     */
    private String cOrderBHRelation;

    /**
     * 物料编码
     */
    private String cMaterielCode;

    /**
     * 行号
     * 同步erp需要
     */
    private String cLineNo;

    /**
     * 发货数量
     */
    private BigDecimal num;
}
