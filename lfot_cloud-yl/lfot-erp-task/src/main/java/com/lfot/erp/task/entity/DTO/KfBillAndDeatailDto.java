package com.lfot.erp.task.entity.DTO;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class KfBillAndDeatailDto {
    /**
     * 采购订单号
     */
    private String cPurchaseBill;
    /**
     * 采购订单详情
     */
    private List<KfBillDeatailDto> kfBillAndDeatail;
}
