package com.lfot.erp.task.service;

import cn.hutool.json.JSONObject;
import com.lfot.erp.task.common.constant.Constants;
import com.lfot.erp.task.common.constant.SoapPostXmlTemplate;
import com.lfot.erp.task.common.enums.PoStatusEnum;
import com.lfot.erp.task.common.enums.TransactionTypeEnum;
import com.lfot.erp.task.common.excetion.SoapErroException;
import com.lfot.erp.task.common.util.*;
import com.lfot.erp.task.entity.DTO.KfBillAndDeatailDto;
import com.lfot.erp.task.entity.DTO.KfBillDeatailDto;
import com.lfot.erp.task.entity.webservice.param.SoapErpComParam;
import com.lfot.erp.task.entity.webservice.response.PoBody;
import com.lfot.erp.task.entity.webservice.response.PoHeader;
import com.lfot.erp.task.entity.webservice.response.PoLine;
import com.lfot.erp.task.entity.webservice.response.SaleOrderLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfot.erp.task.entity.KfBill;
import com.lfot.erp.task.mapper.KfBillMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
@Service
public class KfBillService extends ServiceImpl<KfBillMapper, KfBill> {

    private Logger logger = LoggerFactory.getLogger(KfBillService.class);
    @Autowired
    private  KfBillMapper kfBillMapper;
    @Autowired
    private HttpSoapUtils httpSoapUtils;
    @Autowired
    private SyncRunnable syncRunnable;
    /**
     * 同步采购订单或者退库 只是是退库状态
     */
    public void syncPotoErp() throws Exception {
        //1.查询当天采购数据本数据库的采购订单号，和采购物料编码，及数量 。
        List<KfBillAndDeatailDto> kfBillAndDeatailDos = kfBillMapper.queryKfbillNum(DateUtilS.getDayBegin(), DateUtilS.getDayEnd(), null, (byte) 1, "111");
        if (CollectionUtils.isEmpty(kfBillAndDeatailDos)) {
            return;
        }
        //查询远程erp的订单库
        for (KfBillAndDeatailDto kfBillAndDeatailDo : kfBillAndDeatailDos) {
            String cPurchaseBill = kfBillAndDeatailDo.getCPurchaseBill();
            SoapErpComParam param = new SoapErpComParam();
            param.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
            JSONObject result = httpSoapUtils.postSoap(Constants.SOAP_URL, SoapPostXmlTemplate.queryErpPo(param, cPurchaseBill));
            if (!httpSoapUtils.postStatus(result)) {
                String responseMessage = result.get("X_RESPONSE_MESSAGE", String.class);
                logger.info("执行获取erp采购订单访问失败,失败原因:{}", responseMessage);
                throw new SoapErroException(result.get("X_RESPONSE_MESSAGE", String.class));
            }
            PoBody body = httpSoapUtils.getBanByResponsData(result, PoBody.class);
            if (body == null || body.getHEADER() == null) {
                return;
            }
            List<KfBillDeatailDto> kfBillDeatailDtos = lineCode(body.getHEADER().getLINE(), kfBillAndDeatailDo.getKfBillAndDeatail());
            for (KfBillDeatailDto kfBillDeatailDto : kfBillDeatailDtos) {
                SoapErpComParam cPurchaseBillparam = new SoapErpComParam();
                cPurchaseBillparam.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
                cPurchaseBillparam.setTransactionDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                cPurchaseBillparam.setTransationNumber(Calendar.getInstance().getTimeInMillis() + "");
                String xmlData = SoapPostXmlTemplate.poPutStorage(cPurchaseBillparam, cPurchaseBill, kfBillDeatailDto);
                //进行异步推送
                JSONObject response = syncRunnable.
                        syncPost(Constants.SOAP_URL, xmlData, httpSoapUtils, TransactionTypeEnum.PUTSTORAGE.getCode(), cPurchaseBill, kfBillDeatailDto.getCMaterielCode());

            }
        }
    }

    /**
     * 同步退库
     * @throws Exception
     */
    public void pocancelLint() throws Exception {
        /**
         * 查询当太拟采购数量
         * 传入采购状态 1008
         */
        List<KfBillAndDeatailDto> kfBillAndDeatailDos = kfBillMapper.queryKfbillNum(DateUtilS.getDayBegin(), DateUtilS.getDayEnd(),null,(byte)1,"131");
        if (CollectionUtils.isEmpty(kfBillAndDeatailDos)){
            return ;
        }
        for (KfBillAndDeatailDto kfBillAndDeatailDo : kfBillAndDeatailDos) {
            String cPurchaseBill = kfBillAndDeatailDo.getCPurchaseBill();
            SoapErpComParam param = new SoapErpComParam();
            param.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
            JSONObject result = httpSoapUtils.postSoap(Constants.SOAP_URL, SoapPostXmlTemplate.queryErpPo(param, cPurchaseBill));
            if (!httpSoapUtils.postStatus(result)) {
                String responseMessage = result.get("X_RESPONSE_MESSAGE", String.class);
                logger.info("执行获取erp采购订单访问失败,失败原因:{}", responseMessage);
                throw new SoapErroException(result.get("X_RESPONSE_MESSAGE", String.class));
            }
            PoBody body = httpSoapUtils.getBanByResponsData(result, PoBody.class);
            if (body == null || body.getHEADER() == null) {
                return;
            }
            List<KfBillDeatailDto> kfBillDeatailDtos = lineCode(body.getHEADER().getLINE(), kfBillAndDeatailDo.getKfBillAndDeatail());
            for (KfBillDeatailDto kfBillDeatailDto : kfBillDeatailDtos) {
                //获取入库模板
                SoapErpComParam cPurchaseBillparam = new SoapErpComParam();
                cPurchaseBillparam.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
                cPurchaseBillparam.setTransactionDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                cPurchaseBillparam.setTransationNumber(Calendar.getInstance().getTimeInMillis() + "");
                //获取发送数据模板，进行远程如库操作
                String xmlData = SoapPostXmlTemplate.poCancellingStorage(cPurchaseBillparam, cPurchaseBill, kfBillDeatailDto);
                //进行异步推送
                syncRunnable.syncPost(Constants.SOAP_URL, xmlData, httpSoapUtils, TransactionTypeEnum.CANCELLINT.getCode(), cPurchaseBill, kfBillDeatailDto.getCMaterielCode());
            }
        }
    }

    /**
     * 根据spl的物料编号查取相应的行id
     * @param erpPoLines
     * @param kfBillAndDeatail
     * @return
     */
    public List<KfBillDeatailDto> lineCode(List<PoLine> erpPoLines, List<KfBillDeatailDto> kfBillAndDeatail) {
        List<KfBillDeatailDto> lineNum = new ArrayList<>();
        for (KfBillDeatailDto kfBillDeatailDo : kfBillAndDeatail) {
            String cMaterielCode = kfBillDeatailDo.getCMaterielCode();
            for (PoLine erpPoLine : erpPoLines) {
                if (cMaterielCode.equals(erpPoLine.getITEM_CODE())){
                    kfBillDeatailDo.setLineNum(erpPoLine.getUDF023());
                    lineNum.add(kfBillDeatailDo);
                    break;
                }
            }
        }
        return lineNum;
    }


    /**
     * 同步当天出库数据
     */
    public void poOutbound() throws Exception {
        //查询当天字库转移数据
        List<KfBillAndDeatailDto> kfBillAndDeatailDtos = kfBillMapper.queryKfbillNum(DateUtilS.getDayBegin(), DateUtilS.getDayEnd(), null, (byte) 1, "213");
        if (CollectionUtils.isEmpty(kfBillAndDeatailDtos)){
            return;
        }
        for (KfBillAndDeatailDto kfBillAndDeatailDto : kfBillAndDeatailDtos) {
            List<KfBillDeatailDto> kfBillAndDeatail = kfBillAndDeatailDto.getKfBillAndDeatail();
            if (CollectionUtils.isEmpty(kfBillAndDeatail)){
                return;
            }
            for (KfBillDeatailDto kfBillDeatailDto : kfBillAndDeatail) {
                SoapErpComParam soapErpComParam = new SoapErpComParam();
                soapErpComParam.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
                soapErpComParam.setTransactionDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
                soapErpComParam.setTransationNumber("1310-"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                //获取发送模板
                String xmldata = SoapPostXmlTemplate.getOutBound(soapErpComParam, kfBillDeatailDto);
                syncRunnable.syncPost(Constants.SOAP_URL,xmldata,httpSoapUtils,TransactionTypeEnum.OUTBOUND.getCode(),kfBillAndDeatailDto.getCPurchaseBill(),kfBillDeatailDto.getCMaterielCode());

            }


        }


    }
}
