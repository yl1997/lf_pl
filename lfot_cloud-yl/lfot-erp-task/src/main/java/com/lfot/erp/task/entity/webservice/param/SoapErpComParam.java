package com.lfot.erp.task.entity.webservice.param;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SoapErpComParam {
    /**
     * 传入调用接口批次号 每次调用必须唯一
     */
    private String batchId;
    /**
     * 组织代码
     */
    private String ORGANIZATION_CODE="980";

    /**
     * 最后更新日期 格式(2020-01-01 00:00:01)
     */
    private String LAST_UPDATE_DATE="2021-01-01";

    /**
     * 外部系统接收单号 成功单据号不能重复使用
     */
    private String transationNumber;

    /**
     * <TRANSACTION_TYPE>PO_RECEIVE</TRANSACTION_TYPE>
     * 处理类型
     */
    private String transactionType;

    /**
     * 处理时间 获取当前时间
     */
    private String transactionDate;

    /**
     * 退货出库子库
     */
    private String  rcvSubinventory="CTR01";

    /**
     * 出库货位
     */
    private String  retLocator;
}
