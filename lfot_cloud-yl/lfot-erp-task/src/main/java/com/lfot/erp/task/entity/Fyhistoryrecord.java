package com.lfot.erp.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "fyHistoryRecord")
public class Fyhistoryrecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID",type = IdType.AUTO)
    /**
     * [id]
     */
    private Integer id;

    /**
     * [lcheck]
     */
    @TableField(value = "lCheck")
    private String lcheck;

    /**
     * [cprodid]
     */
    @TableField(value = "cProdID")
    private String cprodid;

    /**
     * [cmaterielcode]
     */
    @TableField(value = "cMaterielCode")
    private String cmaterielcode;

    /**
     * [cbillcode]
     */
    @TableField(value = "cBillCode")
    private String cbillcode;

    /**
     * [nmainnumber]
     */
    @TableField(value = "nMainNumber")
    private Float nmainnumber;

    /**
     * [cmainunit]
     */
    @TableField(value = "cMainUnit")
    private String cmainunit;

    /**
     * [nsecondnumber]
     */
    @TableField(value = "nSecondNumber")
    private Float nsecondnumber;

    /**
     * [csecondunit]
     */
    @TableField(value = "cSecondUnit")
    private String csecondunit;

    /**
     * [nconvertnumber]
     */
    @TableField(value = "nConvertNumber")
    private Float nconvertnumber;

    /**
     * [cconvertunit]
     */
    @TableField(value = "cConvertUnit")
    private String cconvertunit;

    /**
     * [nconvertcoefficient]
     */
    @TableField(value = "nConvertCoefficient")
    private Float nconvertcoefficient;

    /**
     * [ccarnumber]
     */
    @TableField(value = "cCarNumber")
    private String ccarnumber;

    /**
     * [ccartime]
     */
    @TableField(value = "cCarTime")
    private String ccartime;

    /**
     * [doutstoragetime]
     */
    @TableField(value = "dOutStorageTime")
    private Date doutstoragetime;

    /**
     * [doutfactorytime]
     */
    @TableField(value = "dOutFactoryTime")
    private Date doutfactorytime;

    /**
     * [cdeliverybh]
     */
    @TableField(value = "cDeliveryBH")
    private String cdeliverybh;

    /**
     * [ctruckcode]
     */
    @TableField(value = "cTruckCode")
    private String ctruckcode;

    /**
     * [cdurmbh]
     */
    @TableField(value = "cDurmBH")
    private String cdurmbh;

    /**
     * [cdurmbhprod]
     */
    @TableField(value = "cDurmBHProd")
    private String cdurmbhprod;

    /**
     * [dprinttime]
     */
    @TableField(value = "dPrintTime")
    private Date dprinttime;

    /**
     * [cprintoperator]
     */
    @TableField(value = "cPrintOperator")
    private String cprintoperator;

    /**
     * [cprintplus]
     */
    @TableField(value = "cPrintPlus")
    private String cprintplus;

    /**
     * [cmemo]
     */
    @TableField(value = "cMemo")
    private String cmemo;

    /**
     * [cprintprodmodel]
     */
    @TableField(value = "cPrintProdModel")
    private String cprintprodmodel;

    /**
     * [csn]
     */
    @TableField(value = "cSN")
    private String csn;

    /**
     * [cladingbillno]
     */
    @TableField(value = "cLadingBillNo")
    private String cladingbillno;

    /**
     * [ccontainerno]
     */
    @TableField(value = "cContainerNok")
    private String ccontainerno;

    /**
     * [cvolume]
     */
    @TableField(value = "cVolume")
    private Float cvolume;
}
