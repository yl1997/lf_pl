package com.lfot.erp.task.common.enums;

public enum TransactionTypeEnum {
    CANCELLINT("PO_RETURN","退库"),
    PUTSTORAGE("PO_RECEIVE","入库"),
    OUTBOUND("PO_OUT","出库"),
    PICKUPSTORE("PO_PICKUP","挑库"),
    SHIPMENT("PO_SHIPMENT","发运");
    private String code;
    private String msg;

    TransactionTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
