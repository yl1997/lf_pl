package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;

import java.util.List;

@Data
public class CustomerBody {
    private List<CustomerHeader> HEADER;
}
