package com.lfot.erp.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "User_")
public class User implements Serializable {

    private static final long serialVersionUID = -1132928780112201566L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "[name]")
    private String name;

    @TableField(value = "cJobNumber")
    private String cjobnumber;


}