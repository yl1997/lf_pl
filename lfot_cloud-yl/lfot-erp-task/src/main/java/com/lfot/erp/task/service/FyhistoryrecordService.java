package com.lfot.erp.task.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfot.erp.task.common.constant.Constants;
import com.lfot.erp.task.common.constant.SoapPostXmlTemplate;
import com.lfot.erp.task.common.enums.TransactionTypeEnum;
import com.lfot.erp.task.common.util.DateUtilS;
import com.lfot.erp.task.common.util.HttpSoapUtils;
import com.lfot.erp.task.entity.DTO.FyHistoryRecordDto;
import com.lfot.erp.task.entity.Fyhistoryrecord;
import com.lfot.erp.task.entity.KfBill;
import com.lfot.erp.task.entity.webservice.param.SoapErpComParam;
import com.lfot.erp.task.mapper.FyHistoryRecordMapper;
import com.lfot.erp.task.mapper.KfBillMapper;
import com.sun.org.apache.regexp.internal.RE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class FyhistoryrecordService extends ServiceImpl<FyHistoryRecordMapper, Fyhistoryrecord> {

    private Logger logger = LoggerFactory.getLogger(KfBillService.class);

    @Autowired
    private HttpSoapUtils httpSoapUtils;

    @Autowired
    private FyHistoryRecordMapper fyHistoryRecordMapper;

    @Autowired
    private SyncRunnable syncRunnable;

    /**
     * 挑库
     */
    public void pickUpstore() throws Exception {
        List<FyHistoryRecordDto> fyHistoryRecordDtos = fyHistoryRecordMapper.qureyPickUpStoreByDate(DateUtilS.getDayBegin(), DateUtilS.getDayEnd());
        if (CollectionUtils.isEmpty(fyHistoryRecordDtos)){
            return;
        }
        for (FyHistoryRecordDto fyHistoryRecordDto : fyHistoryRecordDtos) {
            //获取销售订单号
            SoapErpComParam param = new SoapErpComParam();
            param.setBatchId(Calendar.getInstance().getTimeInMillis() + "");
            param.setTransactionType(TransactionTypeEnum.CANCELLINT.getCode());
            param.setTransactionDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            param.setTransationNumber("200-OR-"+Calendar.getInstance().getTimeInMillis());
            String postData = SoapPostXmlTemplate.getpickUpstore(param, fyHistoryRecordDto);
            syncRunnable.syncPost(Constants.SOAP_URL,postData,httpSoapUtils,TransactionTypeEnum.PICKUPSTORE.getCode(), fyHistoryRecordDto.getCOrderBHRelation(),fyHistoryRecordDto.getCMaterielCode());

        }


    }

}
