package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class SaleOrderLine {
    /**
     * 新增的行号
     */
    private String LineNumber;

    /**
     * 物料编号
     */
    private String ITEM_CODE;

    /**
     * 业务数量
     */
    private Double BUSINESS_QTY;

    /**
     * 计划发运日期
     */
    private String PLAN_DELIVERY_DATE;

    /**
     * 单价
     */
    private String PRICE;

    /**
     * 金额
     */
    private String AMOUNT;

    /**
     *
     */
    private String FDeliveryPlace;


    /**
     * 段长描述主要解析这个
     */
    private String CAB_LEN;

    /**
     * 行状态
     */
    private String LINE_STATUS;




}
