package com.lfot.erp.task.controller;

import com.lfot.erp.task.common.excetion.RequestfailedException;
import com.lfot.erp.task.common.excetion.SoapErroException;
import com.lfot.erp.task.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("index")
@RestController
public class TesController {
    @Autowired
    private KfBillService kfBillService;

    @Autowired
    private SaleOrderService service;
    @Autowired
    private MainCustomerService mainCustomerService;

    @Autowired
    private FyBillService fyBillService;
    @Autowired
    private MaintProviderService maintProviderService;
    @Autowired
    private FyhistoryrecordService fyhistoryrecordService;

    @GetMapping
    public Object index() throws Exception {
        //kfBillService.SyncPotoErp();
        service.syncSaleOrder();
        return "success";
    }
    @GetMapping("/put")
    public Object put() throws Exception {
        kfBillService.syncPotoErp();
//        service.syncSaleOrder();
        return "success";
    }
    @GetMapping("/cus")
    public Object customer() throws SoapErroException, RequestfailedException {
        mainCustomerService.syncCustomer();
        return "success";
    }
    @GetMapping("/fb")
    public Object fbill() throws Exception {
        fyBillService.syncfyBill();
        return "success";
    }

    @GetMapping("/pro")
    public Object pro() throws SoapErroException, RequestfailedException {
        maintProviderService.syncMainProvider();
        return "success";
    }

    @GetMapping("/kfbill")
    public Object kfbill() throws Exception {
        kfBillService.syncPotoErp();
        return "success";
    }

    @GetMapping("/out")
    public Object poOut() throws Exception {
        kfBillService.poOutbound();
        return "success";
    }
    @GetMapping("/pick")
    public Object pick() throws Exception {
        fyhistoryrecordService.pickUpstore();
        return "success";
    }


}
