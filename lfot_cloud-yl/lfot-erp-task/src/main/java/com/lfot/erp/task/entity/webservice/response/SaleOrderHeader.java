package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class SaleOrderHeader {
    /**
     * 接口调用id
     */
    private String TRANSACTION_ID;
    /**
     * 组织代码
     */
    private String COMPANY_CODE;

    /**
     * 销售订单号
     */
    private String XSOURCE_NO;

    /**
     * 单据类型
     */
    private String DOC_CODE;

    /**
     * 订单类型  ORDER-销售订单,RETURN-退货订单
     */
    private String ORDER_CATEGORY_CODE;

    /**
     * 客户代码
     */
    private String CUSTOMER_CODE;

    /**
     * 采购员
     */
    private String EMPLOYEE_CODEL;

    /**
     *单据日期
     */
    private String DOC_DATE;

    /**
     * CURRENCY_CODE
     */
    private String CURRENCY_CODE;

    /**
     * 收单方
     */
    private String SHIP_CUSTOMER_CODE;

    /**
     * 收货方
     */
    private String INVO_CUSTOMER_CODE;

    /**
     * 客户PO
     */
    private String CUST_PO_NUMBER;

    /**
     * 最后修改时间
     */
    private String LAST_UPDATE_DATE;

    /**
     * 状态
     */
    private String HEADER_STATUS;

    private List<SaleOrderLine> LINE;


}
