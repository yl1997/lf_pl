package com.lfot.erp.task.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfot.erp.task.entity.SaleOrderDetail;
import com.lfot.erp.task.entity.SaleOrderMain;
import com.lfot.erp.task.mapper.SaleOrderDetailMapper;
import com.lfot.erp.task.mapper.SaleOrderMapper;
import org.springframework.stereotype.Service;

@Service
public class SaleOrderDetailService extends ServiceImpl<SaleOrderDetailMapper, SaleOrderDetail> {

}
