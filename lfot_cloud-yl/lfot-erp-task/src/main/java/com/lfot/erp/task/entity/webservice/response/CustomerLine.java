package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;

@Data
public class CustomerLine {
    //工厂编号
    private String CF_COMPANY_CODE;
    //客户名称
    private String CUSTOMER_CODE;
    //客户代码
    private String CUSTOMER_NAME;
    //地址
    private String ADDRESS;
    //税代码
    private String CS_TAX_CLASSIFICATION_CODE;
    //销售人员
    private String CS_EMPLOYEE_CODE;
    //付款条件
    private String CS_PAYMENT_TERM_CODE;
    //联系人
    private String CONTACT;
    //联系电话
    private String PHONE_NUMBER;
    //最后更新日期
    private String LAST_UPDATE_DATE;
}
