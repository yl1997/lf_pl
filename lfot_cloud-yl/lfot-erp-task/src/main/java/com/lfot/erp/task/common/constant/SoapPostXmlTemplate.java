package com.lfot.erp.task.common.constant;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.XmlUtil;
import com.lfot.erp.task.entity.DTO.FyHistoryRecordDto;
import com.lfot.erp.task.entity.DTO.KfBillDeatailDto;
import com.lfot.erp.task.entity.webservice.param.SoapErpComParam;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;

/**
 * webservice请求远程数据模板
 */
public class SoapPostXmlTemplate {
    /**
     * 查询采购信息
     * @param param
     * @return
     */
    public static  String saleOrder(SoapErpComParam param){
        String xmlStr;
        String requestBody;
        try {
            xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/saleOrder.xml")));
            requestBody = StrUtil.format(xmlStr, param.getBatchId(),param.getTransactionDate());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return requestBody;

    }
    public static String queryErpPo(SoapErpComParam param,String cPurchaseBill){
        String xmlStr;
        String requestBody;
        try {
            xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/po.xml")));
            requestBody = StrUtil.format(xmlStr, param.getBatchId(),"",cPurchaseBill);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return requestBody;
    }


    /**
     * 入库
     * @param param
     * @param cPurchaseBill
     * @return
     */
    public static String poPutStorage(SoapErpComParam param, String cPurchaseBill,KfBillDeatailDto KfBillDeatailDo) {
        String xmlStr;
        String requestBody;
        try {
            xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/poPut.xml")));
            requestBody = StrUtil.format(xmlStr, param.getBatchId(), cPurchaseBill,
                    param.getTransationNumber(), param.getTransactionDate(), KfBillDeatailDo.getLineNum(),
                    KfBillDeatailDo.getNum().toString()
            );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return requestBody;
    }

        /**
         * 退库
         * @param param
         * @param cPurchaseBill
         * @return
         */
        public static String poCancellingStorage(SoapErpComParam param, String cPurchaseBill,KfBillDeatailDto KfBillDeatailDo) {
            String xmlStr;
            String requestBody;
            try {
                xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/poCancelling.xml")));
                requestBody = StrUtil.format(xmlStr, param.getBatchId(), cPurchaseBill,
                        param.getTransationNumber(), param.getTransactionDate(), KfBillDeatailDo.getLineNum(),
                        KfBillDeatailDo.getNum().toString()
                );
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;

            }
            return requestBody;
        }



            /**
             * 获取客户信息模板
             * @param param
             * @return
             */
    public static String getCustomer(SoapErpComParam param) {
        String xmlStr;
        String requestBody;
        try {
            xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/maintCustomer.xml")));
            requestBody = StrUtil.format(xmlStr, param.getBatchId(),param.getTransactionDate());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return requestBody;
    }

    /**
     * 发运模板
     * @param param
     * @return
     */
    public static String getfyBill(SoapErpComParam param, FyHistoryRecordDto fyHistoryRecordDto) {
        String xmlStr;
        String requestBody;
        try {
            xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/fyBill.xml")));
             requestBody = StrUtil.format(xmlStr, param.getBatchId(),param.getTransationNumber(),fyHistoryRecordDto.getNum() ,
                    fyHistoryRecordDto.getCLineNo(), fyHistoryRecordDto.getCDeliveryBH());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return requestBody;
    }

    public static String getMainProvider(SoapErpComParam param) {
        String xmlStr;
        String requestBody;
        try {
            xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/mainProvider.xml")));
            requestBody = StrUtil.format(xmlStr, param.getBatchId(),param.getTransactionDate());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return requestBody;
    }

    public static String getOutBound(SoapErpComParam param,KfBillDeatailDto kfBillDeatailDto) {
        String xmlStr;
        String requestBody;
        try {
            xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/subinventoryTransfe.xml")));
            requestBody = StrUtil.format(xmlStr, param.getBatchId(),param.getTransactionDate(),param.getTransationNumber(),kfBillDeatailDto.getCMaterielCode(),kfBillDeatailDto.getNum());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return requestBody;
    }

    /**
     * 获取挑库模板
     * @param param
     * @param
     * @return
     */
    public static String getpickUpstore(SoapErpComParam param, FyHistoryRecordDto fyHistoryRecordDto){
        String xmlStr;
        String requestBody;
        try {
            xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/pickUpstore.xml")));
            requestBody = StrUtil.format(xmlStr, param.getBatchId(),param.getTransactionDate(),param.getTransationNumber(),fyHistoryRecordDto.getCOrderBHRelation(),fyHistoryRecordDto.getCLineNo(),fyHistoryRecordDto.getNum());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return requestBody;


    }
}
