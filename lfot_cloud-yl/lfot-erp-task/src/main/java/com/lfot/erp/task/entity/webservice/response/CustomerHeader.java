package com.lfot.erp.task.entity.webservice.response;

import lombok.Data;

import java.util.List;

@Data
public class CustomerHeader {
    private String TRANSACTION_ID;
    private List<CustomerLine> LINE;
}
