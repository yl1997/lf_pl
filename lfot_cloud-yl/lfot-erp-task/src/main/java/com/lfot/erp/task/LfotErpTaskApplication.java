package com.lfot.erp.task;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
@MapperScan(value = "com.lfot.erp.task.mapper")
@EnableAsync
public class LfotErpTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(LfotErpTaskApplication.class, args);
	}

}
