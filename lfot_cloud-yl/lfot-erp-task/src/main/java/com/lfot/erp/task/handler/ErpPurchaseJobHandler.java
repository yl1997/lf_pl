package com.lfot.erp.task.handler;

import com.lfot.erp.task.service.KfBillService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ERP 采购处理器
 */
@Component
@Slf4j
public class ErpPurchaseJobHandler {
    @Autowired
    private KfBillService kfBillService;

    @XxlJob("syncPotoErp")
    public void syncPotoErp() throws Exception {
        //同步当天的采购入库数据
        kfBillService.syncPotoErp();
    }

    @XxlJob("cancellingStocks")
    public void cancellingStocks() throws Exception {
        //同步当天的采购退库数据
        kfBillService.pocancelLint();
    }
}
