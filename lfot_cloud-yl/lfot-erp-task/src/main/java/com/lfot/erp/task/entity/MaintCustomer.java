package com.lfot.erp.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "maintCustomer")
public class MaintCustomer implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value ="ID", type = IdType.AUTO)
    /**
     * [id]
     */
    private Integer id;

    /**
     * [ctreeno]
     */
    @TableField(value = "cTreeNo")
    private String ctreeno;

    /**
     * [ccode]
     */
    @TableField(value = "cCode")
    private String ccode;

    /**
     * [cname]
     */
    @TableField(value = "cName")
    private String cname;

    /**
     * [cphone]
     */
    @TableField(value = "cPhone")
    private String cphone;

    /**
     * [ccontactman]
     */
    @TableField(value = "cContactman")
    private String ccontactman;

    /**
     * [cfax]
     */
    @TableField(value = "cFax")
    private String cfax;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MaintCustomer that = (MaintCustomer) o;
        return ccode.equals(that.ccode) && cname.equals(that.cname) && cphone.equals(that.cphone) && ccontactman.equals(that.ccontactman) && ccontactaddress.equals(that.ccontactaddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ccode, cname, cphone, ccontactman, ccontactaddress);
    }

    /**
     * [cpostalcode]
     */
    @TableField(value = "cPostalCode")
    private String cpostalcode;

    /**
     * [ccontactaddress]
     */
    @TableField(value = "cContactAddress")
    private String ccontactaddress;

    /**
     * [caccountbank]
     */
    @TableField(value = "cAccountbank")
    private String caccountbank;

    /**
     * [cbanknumber]
     */
    @TableField(value = "cBanknumber")
    private String cbanknumber;

    /**
     * [ctaxnumber]
     */
    @TableField(value = "cTaxNumber")
    private String ctaxnumber;

    /**
     * [ctype]
     */
    @TableField(value = "cType")
    private String ctype;

    /**
     * [cprovince]
     */
    @TableField(value = "cProvince")
    private String cprovince;

    /**
     * [cmemo]
     */
    @TableField(value = "cMemo")
    private String cmemo;

    /**
     * [ledit]
     */
    @TableField(value = "lEdit")
    private String ledit;

    /**
     * [ccodere]
     */
    @TableField(value = "cCodeRE")
    private String ccodere;
}
