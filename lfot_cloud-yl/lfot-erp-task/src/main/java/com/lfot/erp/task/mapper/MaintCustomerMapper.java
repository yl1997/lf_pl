package com.lfot.erp.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfot.erp.task.entity.MaintCustomer;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MaintCustomerMapper extends BaseMapper<MaintCustomer> {
}
