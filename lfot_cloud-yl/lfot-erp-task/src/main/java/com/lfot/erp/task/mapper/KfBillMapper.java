package com.lfot.erp.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfot.erp.task.entity.DTO.KfBillAndDeatailDto;
import com.lfot.erp.task.entity.KfBill;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface KfBillMapper extends BaseMapper<KfBill> {

    /**
     *
     * @param startDate
     * @param endDate
     * @param status 定坛状态 入库 出库1008
     * @param lassessor 审核状态 已审核 1
     * @return
     */
    List<KfBillAndDeatailDto> queryKfbillNum(@Param("startDate") Date startDate,
                                             @Param("endDate") Date endDate,
                                             @Param("status") Integer status,
                                             @Param("lassessors") Byte lassessor,
                                             @Param("ikfBillType") String ikfBillType);






}