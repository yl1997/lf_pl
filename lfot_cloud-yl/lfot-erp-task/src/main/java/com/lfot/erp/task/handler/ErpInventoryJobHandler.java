package com.lfot.erp.task.handler;

import com.lfot.erp.task.service.KfBillService;
import com.xxl.job.core.handler.annotation.XxlJob;
import groovy.transform.AutoImplement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ERP 库存处理器
 */
@Component
@Slf4j
public class ErpInventoryJobHandler {
    @Autowired
    private KfBillService kfBillService;

    @XxlJob("syncPoOutBound")
    public void syncOutbound() throws Exception {
        //同步当天出库数据
        kfBillService.poOutbound();
    }

}
