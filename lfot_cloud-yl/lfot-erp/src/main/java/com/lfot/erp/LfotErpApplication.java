package com.lfot.erp;

import com.bravex.common.security.starter.annotation.EnableBravexCloudResourceServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
@EnableBravexCloudResourceServer
@MapperScan(value = "com.lfot.erp.mapper")
public class LfotErpApplication {

	public static void main(String[] args) {
		SpringApplication.run(LfotErpApplication.class, args);
	}

}
