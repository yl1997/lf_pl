package com.lfot.erp.controller;


import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import com.bravex.common.core.entity.BravexResponse;
import com.lfot.erp.constant.ErpConstant;
import com.lfot.erp.entity.*;
import com.lfot.erp.service.BomService;
import com.lfot.erp.service.ErpService;
import com.lfot.erp.service.SaleOrderDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/wip")
@Api(tags = "ERP 工单 控制器")
public class WipController {

    private final Environment env;
    private final ErpService erpService;
    private final SaleOrderDetailService orderDetailService;
    private final BomService bomService;

    @PostMapping("/getWorkOrderType")
    @ApiOperation(value = "获取ERP工单分类")
    public BravexResponse getWorkOrderType() {
        try {
            List<Map<String, Object>> list = erpService.getWorkOrderType();
            return BravexResponse.ok(list, "查询成功");
        } catch (Exception e) {
            e.printStackTrace();
            return BravexResponse.error("查询失败");
        }
    }

    @PostMapping("/createWorkOrder")
    @ApiOperation(value = "创建ERP工单")
    public BravexResponse createWorkOrder(String orderBh, @RequestParam("cabLen") Double cabLen, @RequestParam("classCode") String classCode, @RequestParam("materielCode") String materielCode, @RequestParam("qty") Double qty) {
        try {
            String xmlStr;
            if (Objects.equals(env.getProperty("current.environment"), "pro")) {
                xmlStr = XmlUtil.toStr(XmlUtil.readXML(ResourceUtils.getFile("classpath:xml/INSERT_WIP_ENTITY_NAME.xml")));
            } else {
                xmlStr = XmlUtil.toStr(XmlUtil.readXML(new ClassPathResource("xml/INSERT_WIP_ENTITY_NAME.xml").getInputStream()));
            }

            String p_batch_id = Calendar.getInstance().getTimeInMillis() + "";

            String requestBody = StrUtil.format(xmlStr, p_batch_id, "ERP-GD-" + p_batch_id, orderBh == null ? "" : orderBh, materielCode, cabLen, classCode, qty);

            String result = HttpRequest.post(ErpConstant.WSDL_URL).header(Header.CONTENT_TYPE, "text/xml;charset=UTF-8")//请求头信息
                    .body(requestBody).timeout(60000)//超时，毫秒
                    .execute().body();

            //1、SAXReader对象
            SAXReader reader = new SAXReader();
            //2、读取XML文件
            Document doc = reader.read(new ByteArrayInputStream(result.getBytes(StandardCharsets.UTF_8)));
            List<Node> nodes = doc.selectNodes("//env:Body//*");
            String status = "";
            String message = "";
            for (Node node : nodes) {
                if (node.getName().equals("X_RESPONSE_STATUS")) {
                    status = node.getText();
                }
                if (node.getName().equals("X_RESPONSE_MESSAGE")) {
                    message = node.getText();
                }
            }

            if (status.equals("S")) {
                return BravexResponse.ok("创建成功");
            } else {
                return BravexResponse.error(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return BravexResponse.error("创建失败");
        }
    }


    //-----------------------   护套工单    -------------------------------------------------------------------------------------------------
    @PostMapping("/getSheathWorkOrderInfo")
    @ApiOperation(value = "获取护套工单信息")
    public BravexResponse getSheathWorkOrderInfo(@RequestParam("orderNo") String orderNo) {

        try {
            //护套工单
            List<Map<String, Object>> list = erpService.getSheathWorkOrderDetail(orderNo);
            return BravexResponse.ok(list, "查询成功");
        } catch (Exception e) {
            e.printStackTrace();
            return BravexResponse.error("查询失败");
        }
    }

    @PostMapping("/getSaleOrderData")
    @ApiOperation(value = "获取合同信息")
    public BravexResponse getSaleOrderData(String cOrderBH, String startTime, String endTime) {

        try {
            List<SaleOrderMainAndDetailData> list = orderDetailService.getSaleOrderData(cOrderBH, startTime, endTime);
            list = list.stream().sorted(Comparator.comparing(SaleOrderMainAndDetailData::getlProdAccept).thenComparing(SaleOrderMainAndDetailData::getcOrderBH)).collect(Collectors.toList());

            for (SaleOrderMainAndDetailData data : list) {
                Fiber_TypeData fiber_data = orderDetailService.getFiberType("1010" + data.getcMaterielCode().trim().substring(7, 9));
                data.setcFiber_type(fiber_data.getFiber_type());
                data.setcFiber_name(fiber_data.getcName());
                data.setcFax("YO");

                data.setFiberKM(NumberUtil.round(data.getFiberKM(), 3).doubleValue());
                data.setnOrderLength(NumberUtil.round(data.getnOrderLength(), 3).doubleValue());
            }

            return BravexResponse.ok(list, "查询成功");
        } catch (Exception e) {
            e.printStackTrace();
            return BravexResponse.error("查询失败");
        }
    }


    //----------------------------  着色工单    --------------------------------------------------------------------------------------------
    @PostMapping("/getColorFiber")
    @ApiOperation(value = "获取着色物料")
    public BravexResponse getColorFiber() {
        try {
            List<ColorFiberWorkOrder> list = bomService.getColorFiber();//可创建着色工单的物料代码
            return BravexResponse.ok(list, "查询成功");
        } catch (Exception e) {
            e.printStackTrace();
            return BravexResponse.error("查询失败");
        }
    }

}
