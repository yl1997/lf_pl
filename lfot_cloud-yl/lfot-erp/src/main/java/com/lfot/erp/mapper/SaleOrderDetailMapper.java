package com.lfot.erp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfot.erp.entity.Fiber_TypeData;
import com.lfot.erp.entity.SaleOrderDetail;
import com.lfot.erp.entity.SaleOrderMainAndDetailData;
import com.lfot.erp.entity.SheathWorkOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SaleOrderDetailMapper extends BaseMapper<SaleOrderDetail> {

    List<SaleOrderMainAndDetailData> getSaleOrderData(@Param("cOrderBH") String cOrderBH, @Param("cTaskCode") String cTaskCode, @Param("startTime") String startTime, @Param("endTime") String endTime);

    Fiber_TypeData getFiberType(@Param("cCode") String cCode);
}