package com.lfot.erp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "kfBillDetail")
public class KfBillDetail implements Serializable {
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "lCheck")
    private Boolean lcheck;

    @TableField(value = "cBillCode")
    private String cbillcode;

    @TableField(value = "cMaterielCode")
    private String cmaterielcode;

    @TableField(value = "iState")
    private Integer istate;

    @TableField(value = "lTest")
    private Boolean ltest;

    @TableField(value = "cOnlyNumber")
    private String conlynumber;

    @TableField(value = "cProdBH")
    private String cprodbh;

    @TableField(value = "cDurmBH")
    private String cdurmbh;

    @TableField(value = "dProdDate")
    private Date dproddate;

    @TableField(value = "cPlaceCode")
    private String cplacecode;

    @TableField(value = "cProviderCode")
    private String cprovidercode;

    @TableField(value = "nMainNumber")
    private Double nmainnumber;

    @TableField(value = "cMainUnit")
    private String cmainunit;

    @TableField(value = "nSecondNumber")
    private Double nsecondnumber;

    @TableField(value = "cSecondUnit")
    private String csecondunit;

    @TableField(value = "nConvertNumber")
    private Double nconvertnumber;

    @TableField(value = "cConvertUnit")
    private String cconvertunit;

    @TableField(value = "nConvertCoefficient")
    private Double nconvertcoefficient;

    @TableField(value = "mPrice")
    private Object mprice;

    @TableField(value = "mMoney")
    private Object mmoney;

    @TableField(value = "cMemo")
    private String cmemo;

    @TableField(value = "dDateTime")
    private Date ddatetime;

    private static final long serialVersionUID = 1L;
}