package com.lfot.erp.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.lfot.erp.entity.ColorFiberWorkOrder;
import com.lfot.erp.mapper.BomMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@Service
@DS("master_1")
public class BomService {
    private final BomMapper bomMapper;

    public List<ColorFiberWorkOrder> getColorFiber() {
        return bomMapper.getColorFiber();
    }

    public String getSubinventoryCode(String materielCode) {
        return bomMapper.getSubinventoryCode(materielCode);
    }
}
