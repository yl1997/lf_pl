package com.lfot.erp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "SaleOrderDetail")
public class SaleOrderDetail implements Serializable {
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "cBillCode")
    private String cbillcode;

    @TableField(value = "lProdAccept")
    private Boolean lprodaccept;

    @TableField(value = "cMaterielCode")
    private String cmaterielcode;

    @TableField(value = "nFiberXS")
    private Integer nfiberxs;

    @TableField(value = "nOrderLength")
    private Double norderlength;

    @TableField(value = "nDurmLength")
    private Double ndurmlength;

    @TableField(value = "cProdCode")
    private String cprodcode;

    @TableField(value = "fPrice")
    private Object fprice;

    @TableField(value = "cProjectName")
    private String cprojectname;

    @TableField(value = "cPrintInformation")
    private String cprintinformation;

    @TableField(value = "nStockDrums")
    private Integer nstockdrums;

    @TableField(value = "nDeliveryDrums")
    private Integer ndeliverydrums;

    @TableField(value = "cTaskCode")
    private String ctaskcode;

    @TableField(value = "cMemo")
    private String cmemo;

    @TableField(value = "cMemo2")
    private String cmemo2;

    @TableField(value = "cProdMemo")
    private String cprodmemo;

    @TableField(value = "cFiberTypeCode")
    private String cfibertypecode;

    @TableField(value = "cReportCode")
    private String creportcode;

    @TableField(value = "nReportCount")
    private Integer nreportcount;

    @TableField(value = "cColorTableCodeFiber")
    private String ccolortablecodefiber;

    @TableField(value = "cColorTableCodeTube")
    private String ccolortablecodetube;

    @TableField(value = "lSelect")
    private Boolean lselect;

    @TableField(value = "cAttCode")
    private String cattcode;

    @TableField(value = "lCoverFilm")
    private Boolean lcoverfilm;

    @TableField(value = "lPlasticPackageHit")
    private Boolean lplasticpackagehit;

    @TableField(value = "cPlasticPackageHitMemo")
    private String cplasticpackagehitmemo;

    @TableField(value = "cDrumType")
    private String cdrumtype;

    @TableField(value = "cDrumMaterial")
    private String cdrummaterial;

    @TableField(value = "cProdRequirement")
    private String cprodrequirement;

    @TableField(value = "cPrintAddMemo")
    private String cprintaddmemo;

    @TableField(value = "cPrintType")
    private String cprinttype;

    private static final long serialVersionUID = 1L;
}