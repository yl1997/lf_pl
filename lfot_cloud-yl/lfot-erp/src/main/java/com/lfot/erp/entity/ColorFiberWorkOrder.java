package com.lfot.erp.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ColorFiberWorkOrder {

    @TableField(value = "cMaterialCode")
    private String cmaterialcode;
    @TableField(value = "cName")
    private String cname;
    @TableField(value = "cModel")
    private String cmodel;

    private BigDecimal workorderqty;//工单数量
}
