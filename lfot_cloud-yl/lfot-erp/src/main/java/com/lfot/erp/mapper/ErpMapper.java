package com.lfot.erp.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ErpMapper {
    List<Map<String, Object>> getWorkOrderType();

    List<Map<String, Object>> getColorFiberWorkOrderInfo(@Param("startTime") String startTime, @Param("endTime") String endTime);

    List<Map<String, Object>> getSheathWorkOrderDetail(@Param("orderNo") String orderNo);
}
