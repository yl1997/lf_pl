package com.lfot.erp.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.*;

import java.math.BigDecimal;

/**
 * 着色有物料代码匹配的生产记录
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ColorFiberProdRecord {

    @TableField(value = "cWorkOrderNum")
    private String cworkordernum;
    @TableField(value = "cMaterielCode")
    private String cmaterielcode;
    @TableField(value = "FiberID")
    private String fiberid;
    @TableField(value = "Length")
    private BigDecimal length;
    @TableField(value = "color")
    private String color;
    @TableField(value = "Opr")
    private String opr;
    @TableField(value = "EquipNo")
    private String equipno;
    @TableField(value = "fib_type")
    private String fibtype;
    @TableField(value = "fib_factory")
    private String fibfactory;
    @TableField(value = "pdate")
    private String pdate;
    @TableField(value = "shift")
    private String shift;
}
