package com.lfot.erp.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SheathWorkOrder {

    private List<Map<String, Object>> workOrderInfo;

    private BigDecimal workOrderQty;

    @TableField(value = "cMaterielCode")
    private String cmaterielcode;

    @TableField(value = "cName")
    private String cname;

    @TableField(value = "cModel")
    private String cmodel;

    @TableField(value = "nOrderLength")
    private BigDecimal norderlength;


}
