package com.lfot.erp.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.lfot.erp.mapper.ErpMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@Service
@DS("master_2")
public class ErpService {
    private final ErpMapper erpMapper;

    public List<Map<String, Object>> getWorkOrderType() {
        return erpMapper.getWorkOrderType();
    }

    public List<Map<String, Object>> getColorFiberWorkOrderInfo(String startTime, String endTime) {
        return erpMapper.getColorFiberWorkOrderInfo(startTime, endTime);
    }

    public List<Map<String, Object>> getSheathWorkOrderDetail(String orderNo) {
        return erpMapper.getSheathWorkOrderDetail(orderNo);
    }
}
