package com.lfot.erp.mapper;

import com.lfot.erp.entity.ColorFiberWorkOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BomMapper {
    List<ColorFiberWorkOrder> getColorFiber();

    String getSubinventoryCode(@Param("materielCode") String materielCode);
}
