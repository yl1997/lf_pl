package com.lfot.erp.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.lfot.erp.entity.Fiber_TypeData;
import com.lfot.erp.entity.SaleOrderMainAndDetailData;
import com.lfot.erp.entity.SheathWorkOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lfot.erp.entity.SaleOrderDetail;
import com.lfot.erp.mapper.SaleOrderDetailMapper;

@Slf4j
@RequiredArgsConstructor
@Service
@DS("master_1")
public class SaleOrderDetailService extends ServiceImpl<SaleOrderDetailMapper, SaleOrderDetail> {


    public List<SaleOrderMainAndDetailData> getSaleOrderData(String cOrderBH, String startTime, String endTime) {
        return baseMapper.getSaleOrderData(cOrderBH, null, startTime, endTime);
    }

    public Fiber_TypeData getFiberType(String cCode) {
        return baseMapper.getFiberType(cCode);
    }
}
