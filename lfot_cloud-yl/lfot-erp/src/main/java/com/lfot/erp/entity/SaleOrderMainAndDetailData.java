package com.lfot.erp.entity;


public class SaleOrderMainAndDetailData {

    private String cOrderBH;
    private String cSortNumber;
    private String lProdAccept;
    private String cMaterielCode;
    private String cModel;
    private String cMaintItem;
    private int nFiberXS;
    private Integer drumCount;
    private String cProdCode;
    private Double nDurmLength;
    private Double nOrderLength;
    private Double FiberKM;
    private String OrderDetailMemo;
    private String YQAtt1310MAX;
    private String YQAtt1550MAX;
    private String cFiberProviderCode;//光纤厂家中文名
    private String cFax;//光纤厂家代号
    private String cFiber_type;//光纤类型
    private String cFiber_name;//光纤名字


    private String YQAtt850MIN;
    private String YQAtt850MAX;
    private String YQAtt1300MIN;
    private String YQAtt1300MAX;
    private String YQAtt1310MIN;

    private String YQAtt1383MIN;
    private String YQAtt1383MAX;
    private String YQAtt1550MIN;

    private String YQAtt1625MIN;
    private String YQAtt1625MAX;
    private String cStateCode;
    private String cStateName;
    private String cBillCode;
    private String cOrderBHRelation;
    private String cBZCode;

    private String dDatetime;
    private String cSalesman;
    private String cOrderType;
    private String cCustomerCode;
    private String CustomerName;
    private String cLinkman;
    private String cLinkPhone;
    private Double nSumLength;
    private Double nXSKM;
    private Double fSumPrice;
    private Double nLengthDeviation;
    private String cPaymentMode;
    private String dDatetimeDelivery;
    private String cReceiverMan;
    private String cPhone;
    private String cAssessor;
    private String cBillMaker;
    private String cMemo;
    private Double fPrice;
    private String cProjectName;
    private String OrderDetailMemo2;
    private boolean lSelect;
    private int nStockDrums;
    private int nDeliveryDrums;
    private String cTaskCode;
    private String cName;
    private boolean lAssessor;
    private String cColorTableFiber;//光纤色谱
    private String cColorTableTube;//束管色谱
    private String cPrintInformation;


    private String dCreateDate;//下达时间
    private String sno;//下达后的序号


    public String getcOrderBH() {
        return cOrderBH;
    }

    public void setcOrderBH(String cOrderBH) {
        this.cOrderBH = cOrderBH;
    }

    public String getcSortNumber() {
        return cSortNumber;
    }

    public void setcSortNumber(String cSortNumber) {
        this.cSortNumber = cSortNumber;
    }

    public String getlProdAccept() {
        return lProdAccept;
    }

    public void setlProdAccept(String lProdAccept) {
        this.lProdAccept = lProdAccept;
    }

    public String getcMaterielCode() {
        return cMaterielCode;
    }

    public void setcMaterielCode(String cMaterielCode) {
        this.cMaterielCode = cMaterielCode;
    }

    public String getcModel() {
        return cModel;
    }

    public void setcModel(String cModel) {
        this.cModel = cModel;
    }

    public String getcMaintItem() {
        return cMaintItem;
    }

    public void setcMaintItem(String cMaintItem) {
        this.cMaintItem = cMaintItem;
    }

    public int getnFiberXS() {
        return nFiberXS;
    }

    public void setnFiberXS(int nFiberXS) {
        this.nFiberXS = nFiberXS;
    }

    public Integer getDrumCount() {
        return drumCount;
    }

    public void setDrumCount(Integer drumCount) {
        this.drumCount = drumCount;
    }

    public String getcProdCode() {
        return cProdCode;
    }

    public void setcProdCode(String cProdCode) {
        this.cProdCode = cProdCode;
    }

    public Double getnDurmLength() {
        return nDurmLength;
    }

    public void setnDurmLength(Double nDurmLength) {
        this.nDurmLength = nDurmLength;
    }

    public Double getnOrderLength() {
        return nOrderLength;
    }

    public void setnOrderLength(Double nOrderLength) {
        this.nOrderLength = nOrderLength;
    }

    public Double getFiberKM() {
        return FiberKM;
    }

    public void setFiberKM(Double fiberKM) {
        FiberKM = fiberKM;
    }

    public String getOrderDetailMemo() {
        return OrderDetailMemo;
    }

    public void setOrderDetailMemo(String orderDetailMemo) {
        OrderDetailMemo = orderDetailMemo;
    }

    public String getYQAtt1310MAX() {
        return YQAtt1310MAX;
    }

    public void setYQAtt1310MAX(String YQAtt1310MAX) {
        this.YQAtt1310MAX = YQAtt1310MAX;
    }

    public String getYQAtt1550MAX() {
        return YQAtt1550MAX;
    }

    public void setYQAtt1550MAX(String YQAtt1550MAX) {
        this.YQAtt1550MAX = YQAtt1550MAX;
    }

    public String getcFiberProviderCode() {
        return cFiberProviderCode;
    }

    public void setcFiberProviderCode(String cFiberProviderCode) {
        this.cFiberProviderCode = cFiberProviderCode;
    }

    public String getcFax() {
        return cFax;
    }

    public void setcFax(String cFax) {
        this.cFax = cFax;
    }

    public String getcFiber_type() {
        return cFiber_type;
    }

    public void setcFiber_type(String cFiber_type) {
        this.cFiber_type = cFiber_type;
    }

    public String getcFiber_name() {
        return cFiber_name;
    }

    public void setcFiber_name(String cFiber_name) {
        this.cFiber_name = cFiber_name;
    }

    public String getYQAtt850MIN() {
        return YQAtt850MIN;
    }

    public void setYQAtt850MIN(String YQAtt850MIN) {
        this.YQAtt850MIN = YQAtt850MIN;
    }

    public String getYQAtt850MAX() {
        return YQAtt850MAX;
    }

    public void setYQAtt850MAX(String YQAtt850MAX) {
        this.YQAtt850MAX = YQAtt850MAX;
    }

    public String getYQAtt1300MIN() {
        return YQAtt1300MIN;
    }

    public void setYQAtt1300MIN(String YQAtt1300MIN) {
        this.YQAtt1300MIN = YQAtt1300MIN;
    }

    public String getYQAtt1300MAX() {
        return YQAtt1300MAX;
    }

    public void setYQAtt1300MAX(String YQAtt1300MAX) {
        this.YQAtt1300MAX = YQAtt1300MAX;
    }

    public String getYQAtt1310MIN() {
        return YQAtt1310MIN;
    }

    public void setYQAtt1310MIN(String YQAtt1310MIN) {
        this.YQAtt1310MIN = YQAtt1310MIN;
    }

    public String getYQAtt1383MIN() {
        return YQAtt1383MIN;
    }

    public void setYQAtt1383MIN(String YQAtt1383MIN) {
        this.YQAtt1383MIN = YQAtt1383MIN;
    }

    public String getYQAtt1383MAX() {
        return YQAtt1383MAX;
    }

    public void setYQAtt1383MAX(String YQAtt1383MAX) {
        this.YQAtt1383MAX = YQAtt1383MAX;
    }

    public String getYQAtt1550MIN() {
        return YQAtt1550MIN;
    }

    public void setYQAtt1550MIN(String YQAtt1550MIN) {
        this.YQAtt1550MIN = YQAtt1550MIN;
    }

    public String getYQAtt1625MIN() {
        return YQAtt1625MIN;
    }

    public void setYQAtt1625MIN(String YQAtt1625MIN) {
        this.YQAtt1625MIN = YQAtt1625MIN;
    }

    public String getYQAtt1625MAX() {
        return YQAtt1625MAX;
    }

    public void setYQAtt1625MAX(String YQAtt1625MAX) {
        this.YQAtt1625MAX = YQAtt1625MAX;
    }

    public String getcStateCode() {
        return cStateCode;
    }

    public void setcStateCode(String cStateCode) {
        this.cStateCode = cStateCode;
    }

    public String getcStateName() {
        return cStateName;
    }

    public void setcStateName(String cStateName) {
        this.cStateName = cStateName;
    }

    public String getcBillCode() {
        return cBillCode;
    }

    public void setcBillCode(String cBillCode) {
        this.cBillCode = cBillCode;
    }

    public String getcOrderBHRelation() {
        return cOrderBHRelation;
    }

    public void setcOrderBHRelation(String cOrderBHRelation) {
        this.cOrderBHRelation = cOrderBHRelation;
    }

    public String getcBZCode() {
        return cBZCode;
    }

    public void setcBZCode(String cBZCode) {
        this.cBZCode = cBZCode;
    }

    public String getdDatetime() {
        return dDatetime;
    }

    public void setdDatetime(String dDatetime) {
        this.dDatetime = dDatetime;
    }

    public String getcSalesman() {
        return cSalesman;
    }

    public void setcSalesman(String cSalesman) {
        this.cSalesman = cSalesman;
    }

    public String getcOrderType() {
        return cOrderType;
    }

    public void setcOrderType(String cOrderType) {
        this.cOrderType = cOrderType;
    }

    public String getcCustomerCode() {
        return cCustomerCode;
    }

    public void setcCustomerCode(String cCustomerCode) {
        this.cCustomerCode = cCustomerCode;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getcLinkman() {
        return cLinkman;
    }

    public void setcLinkman(String cLinkman) {
        this.cLinkman = cLinkman;
    }

    public String getcLinkPhone() {
        return cLinkPhone;
    }

    public void setcLinkPhone(String cLinkPhone) {
        this.cLinkPhone = cLinkPhone;
    }

    public Double getnSumLength() {
        return nSumLength;
    }

    public void setnSumLength(Double nSumLength) {
        this.nSumLength = nSumLength;
    }

    public Double getnXSKM() {
        return nXSKM;
    }

    public void setnXSKM(Double nXSKM) {
        this.nXSKM = nXSKM;
    }

    public Double getfSumPrice() {
        return fSumPrice;
    }

    public void setfSumPrice(Double fSumPrice) {
        this.fSumPrice = fSumPrice;
    }

    public Double getnLengthDeviation() {
        return nLengthDeviation;
    }

    public void setnLengthDeviation(Double nLengthDeviation) {
        this.nLengthDeviation = nLengthDeviation;
    }

    public String getcPaymentMode() {
        return cPaymentMode;
    }

    public void setcPaymentMode(String cPaymentMode) {
        this.cPaymentMode = cPaymentMode;
    }

    public String getdDatetimeDelivery() {
        return dDatetimeDelivery;
    }

    public void setdDatetimeDelivery(String dDatetimeDelivery) {
        this.dDatetimeDelivery = dDatetimeDelivery;
    }

    public String getcReceiverMan() {
        return cReceiverMan;
    }

    public void setcReceiverMan(String cReceiverMan) {
        this.cReceiverMan = cReceiverMan;
    }

    public String getcPhone() {
        return cPhone;
    }

    public void setcPhone(String cPhone) {
        this.cPhone = cPhone;
    }

    public String getcAssessor() {
        return cAssessor;
    }

    public void setcAssessor(String cAssessor) {
        this.cAssessor = cAssessor;
    }

    public String getcBillMaker() {
        return cBillMaker;
    }

    public void setcBillMaker(String cBillMaker) {
        this.cBillMaker = cBillMaker;
    }

    public String getcMemo() {
        return cMemo;
    }

    public void setcMemo(String cMemo) {
        this.cMemo = cMemo;
    }

    public Double getfPrice() {
        return fPrice;
    }

    public void setfPrice(Double fPrice) {
        this.fPrice = fPrice;
    }

    public String getcProjectName() {
        return cProjectName;
    }

    public void setcProjectName(String cProjectName) {
        this.cProjectName = cProjectName;
    }

    public String getOrderDetailMemo2() {
        return OrderDetailMemo2;
    }

    public void setOrderDetailMemo2(String orderDetailMemo2) {
        OrderDetailMemo2 = orderDetailMemo2;
    }

    public boolean islSelect() {
        return lSelect;
    }

    public void setlSelect(boolean lSelect) {
        this.lSelect = lSelect;
    }

    public int getnStockDrums() {
        return nStockDrums;
    }

    public void setnStockDrums(int nStockDrums) {
        this.nStockDrums = nStockDrums;
    }

    public int getnDeliveryDrums() {
        return nDeliveryDrums;
    }

    public void setnDeliveryDrums(int nDeliveryDrums) {
        this.nDeliveryDrums = nDeliveryDrums;
    }

    public String getcTaskCode() {
        return cTaskCode;
    }

    public void setcTaskCode(String cTaskCode) {
        this.cTaskCode = cTaskCode;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public boolean islAssessor() {
        return lAssessor;
    }

    public void setlAssessor(boolean lAssessor) {
        this.lAssessor = lAssessor;
    }

    public String getcColorTableFiber() {
        return cColorTableFiber;
    }

    public void setcColorTableFiber(String cColorTableFiber) {
        this.cColorTableFiber = cColorTableFiber;
    }

    public String getcColorTableTube() {
        return cColorTableTube;
    }

    public void setcColorTableTube(String cColorTableTube) {
        this.cColorTableTube = cColorTableTube;
    }

    public String getcPrintInformation() {
        return cPrintInformation;
    }

    public void setcPrintInformation(String cPrintInformation) {
        this.cPrintInformation = cPrintInformation;
    }

    public String getdCreateDate() {
        return dCreateDate;
    }

    public void setdCreateDate(String dCreateDate) {
        this.dCreateDate = dCreateDate;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }
}
